package org.zaproxy.zap.extension.performanceLog;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class AdditionalInformation {
	
	public static String findAsn() {
		int attempts = 0;
		
		while(attempts < 3) {
			try {			
				URL findAsn = new URL("http://www.telize.com/geoip");
				BufferedReader in = new BufferedReader(new InputStreamReader(findAsn.openStream()));
				String line = in.readLine();
				String[] lines = line.split("\"isp\":");
				if (lines.length <= 0) {
					throw new IOException();
				}
				String[] liness = lines[1].split("\"");
				if (liness.length <= 0) {
					throw new IOException();
				}
				return liness[1];
			} catch (MalformedURLException e) {
				e.printStackTrace();
				return null;
			} catch (IOException e) {
				try {
					Thread.sleep(1300);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
					return null;
				}
				attempts++;
			}
		}
		
		return null;
	}
	
	public static String findPublicIP(String serverAddress) {
		URL whatismyip = null;
		InputStreamReader isr = null;
		HttpsURLConnection urlConnection = null;
		
	try {
		whatismyip = new URL(serverAddress + "/publicip");
		urlConnection = (HttpsURLConnection) whatismyip.openConnection();
		
		
		int respCode = urlConnection.getResponseCode();
		System.out.println("richiesta pip: " + respCode);
		
		if (respCode == HttpsURLConnection.HTTP_OK) { // success
			InputStream is = urlConnection.getInputStream();
			isr = new InputStreamReader(is);
			int numCharsRead;
			char[] charArray = new char[1024];
			StringBuffer sb = new StringBuffer();
			while ((numCharsRead = isr.read(charArray)) > 0) {
				sb.append(charArray, 0, numCharsRead);
			}
			return sb.toString();
		}
		else {
			return null;
		}

	} catch (MalformedURLException e1) {
		e1.printStackTrace();
		return null;
	} catch (IOException e1) {
		return null;
	}
	finally {
		try {
			if (isr != null) {
				isr.close();
			}
			
			urlConnection.disconnect();
		} catch (IOException e) {
			return null;
		}
	}

	}

}
