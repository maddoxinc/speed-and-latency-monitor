/*
 * Zed Attack Proxy (ZAP) and its related class files.
 * 
 * ZAP is an HTTP/HTTPS proxy for assessing web application security.
 * 
 * Copyright 2010 The ZAP development team
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); 
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at 
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0 
 *   
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, 
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 * See the License for the specific language governing permissions and 
 * limitations under the License. 
 */
package org.zaproxy.zap.extension.performanceLog;

import java.awt.EventQueue;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;

import javax.swing.JOptionPane;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import org.apache.log4j.Logger;
import org.parosproxy.paros.Constant;
import org.parosproxy.paros.control.Control;
import org.parosproxy.paros.control.Control.Mode;
import org.parosproxy.paros.extension.ExtensionAdaptor;
import org.parosproxy.paros.extension.ExtensionHook;
import org.parosproxy.paros.extension.ExtensionHookView;
import org.parosproxy.paros.extension.SessionChangedListener;
import org.parosproxy.paros.model.Session;
import org.parosproxy.paros.model.SiteNode;
import org.parosproxy.paros.network.HttpMessage;
import org.zaproxy.zap.extension.help.ExtensionHelp;
import org.zaproxy.zap.extension.httpsessions.ExtensionHttpSessions;
import org.zaproxy.zap.extension.pscan.ExtensionPassiveScan;
import org.zaproxy.zap.view.SiteMapListener;
import org.zaproxy.zap.view.SiteMapTreeCellRenderer;

public class ExtensionParams extends ExtensionAdaptor 
		implements SessionChangedListener, /*ProxyListener, */ SiteMapListener{

	public static final String NAME = "PerformanceLog";
	
	private static int timerInterval = 10000; //ms
	private static int queueCapacity = 10000; //units
	private static int speedThreshold = 20000; //ms
	private static int maxElementsPerMessage = 200; //units
	//
	private ParamsPanel paramsPanel = null;
	private Map <String, HtmlParameterStats> siteParamsMap = new HashMap<String, HtmlParameterStats>();
	
    private Logger logger = Logger.getLogger(ExtensionParams.class);
    
    private ExtensionHttpSessions extensionHttpSessions;
    private ParamScanner paramScanner;
    private ArrayBlockingQueue<Measure> measuresQueue;
    Timer timer;
    boolean timerStarted;
    String client_public_ip;
    String as_num;
    
	/**
     * 
     */
    public ExtensionParams() {
        super();
 		initialize();
    }

    /**
     * @param name
     */
    public ExtensionParams(String name) {
        super(name);
    }

	/**
	 * This method initializes this
	 * 
	 */
	private void initialize() {
        this.setName(NAME);
        this.setOrder(75);
        User user = User.getInstance();
        client_public_ip = AdditionalInformation.findPublicIP("https://" + user.getServerAddress() + ":"+ user.getPort() + "/" + user.getAppName());
        System.out.println(client_public_ip);
        if (client_public_ip == null) {
        	JOptionPane.showMessageDialog(null, "Public IP recognition service not found");
        	System.exit(-7);
        }
        
		as_num = AdditionalInformation.findAsn();
		if (as_num == null) {
			JOptionPane.showMessageDialog(null, "ASN identification service not reachable. Please try again!");
        	System.exit(-8);
		}

        measuresQueue = new ArrayBlockingQueue<Measure>(queueCapacity);
        timer = new Timer();
        timerStarted = false;
        
	}
	
	@Override
	public void hook(ExtensionHook extensionHook) {
	    super.hook(extensionHook);
	    extensionHook.addSessionListener(this);
        extensionHook.addSiteMapListener(this);
	    
	    if (getView() != null) {
	        @SuppressWarnings("unused")
			ExtensionHookView pv = extensionHook.getHookView();
	        extensionHook.getHookView().addStatusPanel(getParamsPanel());

            Control.getSingleton().getExtensionLoader();

	        ExtensionHelp.enableHelpKey(getParamsPanel(), "ui.tabs.params");
	        TableModelListener l = new TableModelListener() {
				
				@Override
				public void tableChanged(TableModelEvent e) {
					
				}
			};
			((ParamsTableModel)paramsPanel.getParamsTable().getModel()).addTableModelListener(l);
	        
	    }

        ExtensionPassiveScan extensionPassiveScan = (ExtensionPassiveScan) Control.getSingleton()
                .getExtensionLoader()
                .getExtension(ExtensionPassiveScan.NAME);
        if (extensionPassiveScan != null) {
            paramScanner = new ParamScanner(this);
            extensionPassiveScan.addPassiveScanner(new ParamScanner(this));
        }
	}
	
	@Override
	public void unload() {
		ExtensionPassiveScan extensionPassiveScan = (ExtensionPassiveScan) Control.getSingleton()
				.getExtensionLoader()
				.getExtension(ExtensionPassiveScan.NAME);
		if (extensionPassiveScan != null) {
			extensionPassiveScan.removePassiveScanner(paramScanner);
		}

		super.unload();
	}
	
	protected ParamsPanel getParamsPanel() {
		if (paramsPanel == null) {
			paramsPanel = new ParamsPanel(this);
		}
		return paramsPanel;
	}
	
	@Override
	public void sessionChanged(final Session session)  {
	    if (EventQueue.isDispatchThread()) {
		    sessionChangedEventHandler(session);

	    } else {
	        try {
	            EventQueue.invokeAndWait(new Runnable() {
	                @Override
	                public void run() {
	        		    sessionChangedEventHandler(session);
	                }
	            });
	        } catch (Exception e) {
	            logger.error(e.getMessage(), e);
	        }
	    }
	}
	
	/**
	 * Gets the ExtensionHttpSessions, if it's enabled
	 * 
	 * @return the Http Sessions extension or null, if it's not available
	 */
	protected ExtensionHttpSessions getExtensionHttpSessions() {
		if(extensionHttpSessions==null){
			extensionHttpSessions = (ExtensionHttpSessions) Control.getSingleton().getExtensionLoader()
					.getExtension(ExtensionHttpSessions.NAME);
		}
		return extensionHttpSessions;
		
	}
	private void sessionChangedEventHandler(Session session) {
		// Clear all scans
		siteParamsMap = new HashMap<String, HtmlParameterStats>();
		this.getParamsPanel().reset();
		if (session == null) {
			// Closedown
			return;
		}
		
		// Repopulate
		SiteNode root = (SiteNode)session.getSiteTree().getRoot();
		@SuppressWarnings("unchecked")
		Enumeration<SiteNode> en = root.children();
		while (en.hasMoreElements()) {
			String site = en.nextElement().getNodeName();
			if (site.indexOf("//") >= 0) {
				site = site.substring(site.indexOf("//") + 2);
			}
			//this.getParamsPanel().addSite(site);
		}
	}

	public boolean onHttpRequestSend(HttpMessage msg) {
				
		return true;
	}
	

	public boolean onHttpResponseReceive(HttpMessage msg) {
		System.out.println(msg.getRequestHeader().getURI());
		System.out.println("B: " + msg.getTtfb());
		System.out.println("RR: " + msg.getRespsize());
		String remote_server_ip = null;
		int download_speed = 0;
		

		try {
			remote_server_ip = InetAddress.getByName(msg.getRequestHeader().getHostName()).getHostAddress();
		} catch (UnknownHostException e1) {
			//Ignoring bad measure against an unknown host
			return true;
		}
		long duration = 0;
		
		//If response body is present, compute download time
		if((long)msg.getResponseBody().length() != 0) {
			duration = msg.getTimeElapsedMillis()+msg.getTimeSentMillis()-msg.getTtfb();
		}
		User u = User.getInstance();

		if (duration >= speedThreshold) {
			download_speed = (int)(msg.getRespsize()/duration)*8/1000;
		}
		
		//Getting only the real domain from the full host name
		String[] subdomains = msg.getRequestHeader().getHostName().split("\\.");
		String domain = null;
		if (subdomains[subdomains.length - 1] != null && subdomains[subdomains.length - 2] != null) {
			domain = subdomains[subdomains.length - 2] + "." + subdomains[subdomains.length - 1];
		}
		else {
			domain = msg.getRequestHeader().getHostName();
		}
		
		Measure m1 = new Measure(null, client_public_ip, as_num, remote_server_ip, domain, msg.getRequestHeader().getURI().toString(), (long)msg.getRespsize(), duration, download_speed, msg.getTtfb()-msg.getTimeSentMillis(), msg.getResponseHeader().getHeader("Content-Type"), msg.getTimeSentMillis(), u.getID());
		
		if (!User.getInstance().getLogged()) {
			//Redo login
			Functions.formLogin(null);
		}
		
		try {
			synchronized (measuresQueue) {
				if (!measuresQueue.offer(m1, timerInterval/2, TimeUnit.MILLISECONDS)) {
					return true;
				}
			}
		} catch (InterruptedException e1) { //Launched if interrupted while waiting
			return false;
		}
		
		if (!timerStarted) { //Check if first message after sending a packet
			timer.schedule(new MeasuresSender(measuresQueue, paramsPanel, this, maxElementsPerMessage), timerInterval);
			setTimerStarted(true);
		}
		
		return true;
	}

	@Override
	public void nodeSelected(SiteNode node) {
		// Event from SiteMapListenner
	}

	@Override
	public void onReturnNodeRendererComponent(
			SiteMapTreeCellRenderer component, boolean leaf, SiteNode value) {
	}
	
	public Map<String, HtmlParameterStats> getSiteParamsMap() {
		return siteParamsMap;
	}

	public HtmlParameterStats getSelectedParam() {
		return this.getParamsPanel().getSelectedParam();
	}

	@Override
	public void sessionAboutToChange(Session session) {
	}

	@Override
	public void sessionScopeChanged(Session session) {
	}

	@Override
	public String getAuthor() {
		return "Quelli della FEZ";
	}

	@Override
	public String getDescription() {
		return Constant.messages.getString("performance.desc");
	}

	@Override
	public URL getURL() {
		try {
			return new URL(Constant.ZAP_HOMEPAGE);
		} catch (MalformedURLException e) {
			return null;
		}
	}
	
	@Override
	public void sessionModeChanged(Mode mode) {
		// Ignore
	}
	
	public void setTimerStarted(boolean value) {
		this.timerStarted = value;
	}
}
