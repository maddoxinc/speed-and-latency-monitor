package org.zaproxy.zap.extension.performanceLog;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.GeneralSecurityException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.ArrayList;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import org.parosproxy.paros.extension.Extension;

public class Functions {

	
	public static void lenientHttps()
	{
	    TrustManager[] trustAllCertificates = new TrustManager[] {
	        new X509TrustManager() {
	            @Override
	            public X509Certificate[] getAcceptedIssuers() {
	                return null; // Not relevant.
	            }
	            @Override
	            public void checkClientTrusted(X509Certificate[] certs, String authType) {
	                // Do nothing. Just allow them all.
	            }
	            @Override
	            public void checkServerTrusted(X509Certificate[] certs, String authType) {
	                // Do nothing. Just allow them all.
	            }
	        }
	    };
	 

	    HostnameVerifier trustAllHostnames = new HostnameVerifier() {
	        @Override
	        public boolean verify(String hostname, SSLSession session) {
	            return true; // Just allow them all.
	        }
	    };

	    try {
	        System.setProperty("jsse.enableSNIExtension", "false");
	        SSLContext sc = SSLContext.getInstance("SSL");
	        sc.init(null, trustAllCertificates, new SecureRandom());
	        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
	        HttpsURLConnection.setDefaultHostnameVerifier(trustAllHostnames);
	    }
	    catch (GeneralSecurityException e) {
	        throw new ExceptionInInitializerError(e);
	    }
	}
	
	
	public static void createConfPopup(String filePathString) throws FileNotFoundException, UnsupportedEncodingException {
		//create popup for configurations
	    JTextField serverAddress = new JTextField();
	    JTextField port = new JTextField();
	    JTextField appName = new JTextField();

	    JPanel panel = new JPanel(new GridLayout(0, 1));
	    panel.add(new JLabel("Server Address:"));
	    panel.add(serverAddress);
	    panel.add(new JLabel("Port:"));
	    panel.add(port);
	    panel.add(new JLabel("Application Name:"));
	    panel.add(appName);
		
		Path path = Paths.get(filePathString);

		//force user to provide configuration

		if(!Files.exists(path))
		{
			PrintWriter writer = null;
			try
			{
				boolean flagConfig = false;

			    while(!flagConfig)
			    {
				    int result = JOptionPane.showConfirmDialog(null, panel, "Configuration",
					        JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
				    if (result == JOptionPane.OK_OPTION) {
				    	
				    	String sa = serverAddress.getText();
				    	String po = port.getText();
				    	String an = appName.getText();
				    	
				    	if(sa.length()>0 && po.length()>0 && an.length()>0)
				    	{
					        writer = new PrintWriter(filePathString, "UTF-8");
					    	writer.println("serverAddress:"+ sa);
					    	writer.println("port:"+ po);
					    	writer.println("appName:"+ an);
					    	flagConfig = true;
				    	}
				    	else
				    	{
					    	JOptionPane.showMessageDialog(null, "Please fill all the fields", "Warning Message",
					                JOptionPane.WARNING_MESSAGE);
					    	flagConfig = false;
				    	}
				    } 
				    else
				    {
				    	JOptionPane.showMessageDialog(null, "Configuration is mandatory", "Warning Message",
				                JOptionPane.WARNING_MESSAGE);
				    	flagConfig = false;
				    }
			    }
			}
			finally{
				writer.close();
			}
			
		}
	}
	
	public static void readConfFile(String filePathString) throws IOException {
		BufferedReader br = null;
		//retrieve configuration from file
		br = new BufferedReader(new FileReader(filePathString));
		String sCurrentLine;
		String[] parameters = new String[4];
		int i=0;
		while ((sCurrentLine = br.readLine()) != null) {
			parameters[i++] = sCurrentLine;								
		}
		
		String[] info;
		//Server address
		User u = User.getInstance();
		info = parameters[0].split(":");
		u.setServerAddress(info[1]);
		//port
		info = parameters[1].split(":");
		u.setPort(info[1]);
		//appName
		info = parameters[2].split(":");
		u.setAppName(info[1]);
		
		br.close();
	}
	
	public static void recreateConfPopup(JButton butt) {
		boolean flagConfig = false;
		PrintWriter writer = null;
		String filePathString = "src/org/zaproxy/zap/extension/performanceLog/Config.txt";
		BufferedReader br = null;
		
		try
		{
			//create popup for configurations
		    JTextField serverAddress = new JTextField();
		    JTextField port = new JTextField();
		    JTextField appName = new JTextField();

		    JPanel panel = new JPanel(new GridLayout(0, 1));
		    panel.add(new JLabel("Server Address:"));
		    panel.add(serverAddress);
		    panel.add(new JLabel("Port:"));
		    panel.add(port);
		    panel.add(new JLabel("Application Name:"));
		    panel.add(appName);
		    
			while(!flagConfig){
				int result = JOptionPane.showConfirmDialog(null, panel, "Configuration",
				        JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
			    if (result == JOptionPane.OK_OPTION) {
			    	
			    	String sa = serverAddress.getText();
			    	String po = port.getText();
			    	String an = appName.getText();
			    	
			    	if(sa.length()>0 && po.length()>0 && an.length()>0)
			    	{
				        writer = new PrintWriter(filePathString, "UTF-8");
				    	writer.println("serverName:"+ sa);
				    	writer.println("port:"+ po);
				    	writer.println("appName:"+ an);
				    	flagConfig = true;
				    	writer.close();
			    	}
			    	
					br = new BufferedReader(new FileReader(filePathString));
					String sCurrentLine;
					String[] parameters = new String[4];
					int i=0;
					while ((sCurrentLine = br.readLine()) != null) {
						parameters[i++] = sCurrentLine;								
					}
					
					String[] info;
					User u = User.getInstance();
					//Server address
					info = parameters[0].split(":");
					u.setServerAddress(info[1]);
					//port
					info = parameters[1].split(":");
					u.setPort(info[1]);
					//appName
					info = parameters[2].split(":");
					u.setAppName(info[1]);
					
					br.close();
			    } 
			    else
			    {
			    	flagConfig = true;
			    }
			}	
		} catch (FileNotFoundException e1) {
    		JOptionPane.showMessageDialog(null, "Please provide a configuration!", "Error Message",
                    JOptionPane.ERROR_MESSAGE);
		} catch (UnsupportedEncodingException e1) {
    		JOptionPane.showMessageDialog(null, "Please provide a configuration!!", "Error Message",
                    JOptionPane.ERROR_MESSAGE);
		} catch (IOException e1) {
    		JOptionPane.showMessageDialog(null, "Please provide a configuration!!!", "Error Message",
                    JOptionPane.ERROR_MESSAGE);
		}
	}
	
	public static void formLogin(ArrayList<Extension> extensions) {
		//credentials
	    JTextField field1 = new JTextField();

	    JPasswordField field2 = new JPasswordField();
	    final JButton butt = new JButton("Configuration");
	    JPanel panelLogin = new JPanel(new GridLayout(3,2));
	    panelLogin.add(new JLabel("Username:"));
	    panelLogin.add(field1);
	    panelLogin.add(new JLabel("Password:"));
	    panelLogin.add(field2);
	    panelLogin.add(butt);
	    
		boolean flagLogin = false;
		User u = User.getInstance();
		
		//Listener on Config button
	    butt.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				Functions.recreateConfPopup(butt);
			}
		});
	    
	    
		while(!flagLogin){
		    int result = JOptionPane.showConfirmDialog(null, panelLogin, "Authentication",
		        JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
		    if (result == JOptionPane.OK_OPTION) {
		    	System.out.println(field2.getPassword());
		    	//check credentials
		    	
		    	String user = field1.getText();
		    	String pass = String.valueOf(field2.getPassword());
		    	int returnCode;
		    	if(user.length() > 0 && pass.length() > 0) {
		    		if( (returnCode = Login.get(user,pass)) == 0) {
		    			u.setLogged(true);
				    	flagLogin = true;
				    	if (extensions != null) {
				    		extensions.add(new org.zaproxy.zap.extension.performanceLog.ExtensionParams());
				    	}
				        continue;
		    		}
		    		else if (returnCode == -1) //Wrong credentials
			    	{
			    		JOptionPane.showMessageDialog(null, "Wrong credentials", "Error Message",
			                    JOptionPane.ERROR_MESSAGE);
			    		continue;
			    	}
		    		else if (returnCode == -2) { //Wrong URL
		    			JOptionPane.showMessageDialog(null, "Wrong URL Syntax - Please provide a correct configuration", "Error Message",
			                    JOptionPane.ERROR_MESSAGE);
		    			u.setLogged(false);
				    	flagLogin = false;
		    		}
		    		else if(returnCode == -4) {
		    			JOptionPane.showMessageDialog(null, "server just had a hiccup", "Error Message",
			                    JOptionPane.ERROR_MESSAGE);
		    			u.setLogged(false);
		    		}
		    		else { //Network Error
		    			JOptionPane.showMessageDialog(null, "Network error or server not reachable - Plugin deactivated", "Error Message",
			                    JOptionPane.ERROR_MESSAGE);
		    			u.setLogged(false);
				    	flagLogin = true;
		    		}
		    	}
		    	
		    } else {
		    	JOptionPane.showMessageDialog(null, "Proxy will run without the PerformanceLog plugin", "Warning Message",
		                JOptionPane.WARNING_MESSAGE);
		    	u.setLogged(false);
		    	flagLogin = true;
		    }
	    }
	}
}
