/*
 * Zed Attack Proxy (ZAP) and its related class files.
 * 
 * ZAP is an HTTP/HTTPS proxy for assessing web application security.
 * 
 * Copyright 2010 The ZAP development team
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); 
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at 
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0 
 *   
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, 
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 * See the License for the specific language governing permissions and 
 * limitations under the License. 
 */
package org.zaproxy.zap.extension.performanceLog;


public class HtmlParameterStats implements Comparable<HtmlParameterStats> {
	private long id = -1;
	private long timestamp;
	private int numitens;
	
	public HtmlParameterStats(long timestamp, int numitems) {
		this.timestamp = timestamp;
		this.numitens = numitems;
	}
	
	public HtmlParameterStats(long id, long timestamp, int numitems) {
		this.id = id;
		this.timestamp = timestamp;
		this.numitens = numitems;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public int getNumitens() {
		return numitens;
	}

	public void setNumitens(int numitens) {
		this.numitens = numitens;
	}

	@Override
	public int compareTo(HtmlParameterStats o) {
		if (o == null) { 
			return 1;
		}
		int result = (int) (this.timestamp - o.getTimestamp());
		if (result == 0) {
			// Same type
			result = this.numitens - o.getNumitens();
		}
		return result;
	}
}
