package org.zaproxy.zap.extension.performanceLog;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ConnectException;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import org.apache.commons.codec.binary.Base64;

public class Login {
	
	public static int get(String username, String password){	
		
		User user = User.getInstance();
		HttpsURLConnection urlConnection = null;
		try 
		{
			URL url = null;
			if(user.getServerAddress()!= null && user.getPort()!=null && user.getAppName()!=null )
			{
				String urlString = "https://" + user.getServerAddress() + ":"+ user.getPort() + "/" + user.getAppName() + "/user";
				System.out.println(urlString);
				url = new URL(urlString);
			}
			
			String authString = username + ":" + password;
			System.out.println("auth string: " + authString);
			byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
			String authStringEnc = new String(authEncBytes);
			System.out.println("Base64 encoded auth string: " + authStringEnc);
			
			CookieManager custom = new CookieManager(null, CookiePolicy.ACCEPT_ALL);
			CookieHandler.setDefault(custom);
			
			urlConnection = (HttpsURLConnection) url.openConnection();
			urlConnection.setRequestProperty("Authorization", "Basic " + authStringEnc);
			
			int respCode = urlConnection.getResponseCode();
			System.out.println("prima richiesta: " + respCode);
			
			if (respCode == HttpsURLConnection.HTTP_OK) { // success
				InputStream is = urlConnection.getInputStream();
				InputStreamReader isr = new InputStreamReader(is);
				int numCharsRead;
				char[] charArray = new char[1024];
				StringBuffer sb = new StringBuffer();
				while ((numCharsRead = isr.read(charArray)) > 0) {
					sb.append(charArray, 0, numCharsRead);
				}

				String result = sb.toString();
				
				String[] lines = result.split("\\{\"id\":");
				if (lines.length <= 1) {
					throw new IOException();
				}
				String[] liness = lines[1].split(",");
				if (liness.length <= 1) {
					throw new IOException();
				}
				
				user.setID(Long.valueOf(liness[0]));
		    	user.setUsername(username);
				user.setCookieManager(custom);
				
			}
			else if ((respCode == HttpURLConnection.HTTP_FORBIDDEN) || (respCode == HttpURLConnection.HTTP_UNAUTHORIZED)){
				return -1;
			}
			else {
				return -4;
			}
			
		} catch (MalformedURLException e) {
			return -2;
		} 
		catch (ConnectException e) {
			return -3;
		}
		catch (IOException e) {
			return -3;
		} 
		finally{
			try
			{
				//lancia eccezione in caso di malformed URL exception
				urlConnection.disconnect();
			}
			catch(Exception e){
				return -2;
			}
		}
		
		URL url1;
		HttpsURLConnection urlConnection1 = null;
		try {
			//CookieManager custom = new CookieManager(null, CookiePolicy.ACCEPT_ALL);
			// First set the default cookie manager.
			//CookieHandler.setDefault(custom);
			//User.getInstance().setCookieManager(custom);
			url1 = new URL("https://" + user.getServerAddress() + ":"+ user.getPort() + "/" + user.getAppName() + "/resources/index.html");
			urlConnection1 = (HttpsURLConnection) url1.openConnection();
			
			int resp = urlConnection1.getResponseCode();
			System.out.println("seconda richiesta: " + resp);
			//System.out.println(extractXrsfToken(urlConnection));
			//user.setCsrf(extractXrsfToken(urlConnection));
		} catch (MalformedURLException e) {
			return -2;
		}
		catch (ConnectException e) {
			return -3;
		}
		catch (IOException e) {
			return -3;
		}
		try
		{
			//lancia eccezione in caso di malformed URL exception
			urlConnection1.disconnect();
		}
		catch(Exception e){
			return -2;
		}
		
		return 0;
		
	}

}
