package org.zaproxy.zap.extension.performanceLog;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpCookie;
import java.net.URL;
import java.util.ArrayList;
import java.util.TimerTask;
import java.util.concurrent.ArrayBlockingQueue;

import javax.net.ssl.HttpsURLConnection;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;


public class MeasuresSender extends TimerTask {
	private ArrayBlockingQueue<Measure> measuresQueue;
	private ParamsPanel paramsPanel;
	private ExtensionParams extparam;
	private int maxElementsPerMessage;
	
	public MeasuresSender(ArrayBlockingQueue<Measure> measuresQueue, ParamsPanel paramsPanel, ExtensionParams extparam, int maxElementsPerMessage) {
		this.measuresQueue = measuresQueue;
		this.paramsPanel = paramsPanel;
		this.extparam = extparam;
		this.maxElementsPerMessage = maxElementsPerMessage;
	}
	
	@Override
	public void run() {
		ArrayList<Measure> measuresList = new ArrayList<Measure>();
		synchronized (measuresQueue) {
			measuresQueue.drainTo(measuresList, maxElementsPerMessage);
		}
		
		sendMessage(measuresList);
		extparam.setTimerStarted(false);
		this.cancel();
	}
	
	private void sendMessage(ArrayList<Measure> measuresList) {
		
		ObjectMapper mapper = new ObjectMapper();
		String jsonList;
		
		try {
			jsonList = mapper.writeValueAsString(measuresList);
			User user = User.getInstance();
			HttpsURLConnection httpcon = (HttpsURLConnection) ((new URL("https://" + user.getServerAddress() + ":"+ user.getPort() + "/" + user.getAppName() + "/measures").openConnection()));
			httpcon.setDoOutput(true);
			
			for (HttpCookie cookie : User.getInstance().getCookieManager().getCookieStore().getCookies()) {
				if(cookie.getName().compareToIgnoreCase("XSRF-TOKEN") == 0) {
					System.out.println("valore cookie: " + cookie.getValue());
					httpcon.setRequestProperty("X-XSRF-TOKEN", cookie.getValue());
				}
			}
			
			httpcon.setRequestProperty("Content-Type", "application/json");
			httpcon.setRequestProperty("Accept", "application/json");
			httpcon.setRequestMethod("POST");
			httpcon.setRequestProperty("Content-length", String.valueOf(jsonList.length()));
			httpcon.connect();

			byte[] outputBytes = jsonList.getBytes("UTF-8");
			OutputStream os = (OutputStream) httpcon.getOutputStream();
			os.write(outputBytes);
			
			System.out.println("stampa");
			os.close();
			int result = httpcon.getResponseCode();
			System.out.println(result);
			
			if (result != 201) {
				HtmlParameterStats hp = new HtmlParameterStats(System.currentTimeMillis(), -1);
				((ParamsTableModel)paramsPanel.getParamsTable().getModel()).addHtmlParameterStats(hp);
				
				if(result == 401 || result == 403){
					user.setLogged(false);
				}
			}
			else {
				HtmlParameterStats hp = new HtmlParameterStats(System.currentTimeMillis(), measuresList.size());
				((ParamsTableModel)paramsPanel.getParamsTable().getModel()).addHtmlParameterStats(hp);
			}
			return;

		} catch (JsonGenerationException e1) {
			HtmlParameterStats hp = new HtmlParameterStats(System.currentTimeMillis(), -6);
			((ParamsTableModel)paramsPanel.getParamsTable().getModel()).addHtmlParameterStats(hp);
		} catch (JsonMappingException e1) {
			HtmlParameterStats hp = new HtmlParameterStats(System.currentTimeMillis(), -5);
			((ParamsTableModel)paramsPanel.getParamsTable().getModel()).addHtmlParameterStats(hp);
		} catch (IOException e1) {
			HtmlParameterStats hp = new HtmlParameterStats(System.currentTimeMillis(), -1);
			((ParamsTableModel)paramsPanel.getParamsTable().getModel()).addHtmlParameterStats(hp);
			return;
		}
	}
}
