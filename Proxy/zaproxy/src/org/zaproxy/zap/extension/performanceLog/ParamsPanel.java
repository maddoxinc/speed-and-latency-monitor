/*
 * Zed Attack Proxy (ZAP) and its related class files.
 * 
 * ZAP is an HTTP/HTTPS proxy for assessing web application security.
 * 
 * Copyright 2010 psiinon@gmail.com
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); 
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at 
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0 
 *   
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, 
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 * See the License for the specific language governing permissions and 
 * limitations under the License. 
 */
package org.zaproxy.zap.extension.performanceLog;

import java.awt.CardLayout;
import java.awt.Event;
import java.awt.GridBagConstraints;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;

import org.jdesktop.swingx.JXTable;
import org.parosproxy.paros.Constant;
import org.parosproxy.paros.extension.AbstractPanel;

public class ParamsPanel extends AbstractPanel{
	
	private static final long serialVersionUID = 1L;

	public static final String PANEL_NAME = "params";
	
	private ExtensionParams extension = null;
	private JPanel panelCommand = null;
	private JScrollPane jScrollPane = null;

	private JXTable paramsTable = null;
	private ParamsTableModel paramsModel = new ParamsTableModel();
    
    /**
     * 
     */
    public ParamsPanel(ExtensionParams extension) {
        super();
        this.extension = extension;
 		initialize();
    }

	/**
	 * This method initializes this
	 */
	private  void initialize() {
        this.setLayout(new CardLayout());
        this.setSize(474, 251);
        this.setName(Constant.messages.getString("performance.panel.title"));
		this.setIcon(new ImageIcon(ParamsPanel.class.getResource("/resource/icon/16/147.png")));	// 'form' icon
		this.setDefaultAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_L, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask() | Event.SHIFT_MASK, false));
		this.setMnemonic(Constant.messages.getChar("params.panel.mnemonic"));
        this.add(getPanelCommand(), getPanelCommand().getName());
	}
	/**

	 * This method initializes panelCommand	

	 * 	

	 * @return javax.swing.JPanel	

	 */    
	/**/
	private javax.swing.JPanel getPanelCommand() {
		if (panelCommand == null) {

			panelCommand = new javax.swing.JPanel();
			panelCommand.setLayout(new java.awt.GridBagLayout());
			panelCommand.setName("Perf");
			
			GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
			GridBagConstraints gridBagConstraints2 = new GridBagConstraints();

			gridBagConstraints1.gridx = 0;
			gridBagConstraints1.gridy = 0;
			gridBagConstraints1.insets = new java.awt.Insets(2,2,2,2);
			gridBagConstraints1.anchor = java.awt.GridBagConstraints.NORTHWEST;
			gridBagConstraints1.fill = java.awt.GridBagConstraints.HORIZONTAL;
			gridBagConstraints1.weightx = 1.0D;
			gridBagConstraints2.gridx = 0;
			gridBagConstraints2.gridy = 1;
			gridBagConstraints2.weightx = 1.0;
			gridBagConstraints2.weighty = 1.0;
			gridBagConstraints2.fill = java.awt.GridBagConstraints.BOTH;
			gridBagConstraints2.insets = new java.awt.Insets(0,0,0,0);
			gridBagConstraints2.anchor = java.awt.GridBagConstraints.NORTHWEST;
			
			panelCommand.add(getJScrollPane(), gridBagConstraints2);
			
		}
		return panelCommand;
	}

	private JScrollPane getJScrollPane() {
		if (jScrollPane == null) {
			jScrollPane = new JScrollPane();
			jScrollPane.setViewportView(getParamsTable());
		}
		return jScrollPane;
	}
	
	private void setParamsTableColumnSizes() {
		
		paramsTable.getColumnModel().getColumn(0).setMinWidth(50);
		paramsTable.getColumnModel().getColumn(0).setPreferredWidth(100);	// type
		
		paramsTable.getColumnModel().getColumn(1).setMinWidth(100);
		paramsTable.getColumnModel().getColumn(1).setPreferredWidth(200);	// name
		
	}
	
	protected JXTable getParamsTable() {
		if (paramsTable == null) {
			paramsTable = new JXTable(paramsModel);
			paramsTable.setColumnSelectionAllowed(false);
			paramsTable.setCellSelectionEnabled(false);
			paramsTable.setRowSelectionAllowed(true);
			paramsTable.setAutoCreateRowSorter(true);
			paramsTable.setColumnControlVisible(true);

			this.setParamsTableColumnSizes();

			paramsTable.setName(PANEL_NAME);
			paramsTable.setDoubleBuffered(true);
			paramsTable.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
			
			paramsTable.addMouseListener(new java.awt.event.MouseAdapter() { 
			    @Override
			    public void mousePressed(java.awt.event.MouseEvent e) {
					//showPopupMenuIfTriggered(e);
				}
				@Override
				public void mouseReleased(java.awt.event.MouseEvent e) {
					//showPopupMenuIfTriggered(e);
				}

				/*private void showPopupMenuIfTriggered(java.awt.event.MouseEvent e) {
					if (e.isPopupTrigger()) {

						// Select table item
					    int row = paramsTable.rowAtPoint( e.getPoint() );
					    if ( row < 0 || !paramsTable.getSelectionModel().isSelectedIndex( row ) ) {
					    	paramsTable.getSelectionModel().clearSelection();
					    	if ( row >= 0 ) {
					    		paramsTable.getSelectionModel().setSelectionInterval( row, row );
					    	}
					    }
						
						View.getSingleton().getPopupMenu().show(e.getComponent(), e.getX(), e.getY());
			        }
			    }*/
			});
		}
		return paramsTable;
	}

	public void reset() {
		paramsModel.removeAllElements();
		paramsModel.fireTableDataChanged();
		
		paramsTable.setModel(paramsModel);
		extension.getSiteParamsMap().clear();

	}
	
	protected HtmlParameterStats getSelectedParam() {
		
		long item_timestamp = (long)this.getParamsTable().getValueAt(this.getParamsTable().getSelectedRow(), 0);
		
		return extension.getSiteParamsMap().get(item_timestamp);
	}
}
