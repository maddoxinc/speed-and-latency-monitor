package org.zaproxy.zap.extension.performanceLog;

import java.net.CookieManager;

public class User {
	
	private  String username;
	private long id;
	private boolean isLoggedIn = false;
	private CookieManager cookieManager;
	private String serverAddress;
	private String port;
	private String appName;
	
	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getServerAddress() {
		return serverAddress;
	}

	public void setServerAddress(String serverAddress) {
		this.serverAddress = serverAddress;
	}

	private static User u;
	
	private User(){}
	
	public static User getInstance(){
		if(u == null)
			u = new User();
		
		return u;
	}
	
	public void setUsername(String user){
		username = user;
	}
	
	public void setID(long ID){
		 id = ID;
	}
	
	public void setLogged(boolean status){
		isLoggedIn = status;
	}
	
	public CookieManager getCookieManager() {
		return cookieManager;
	}

	public void setCookieManager(CookieManager cookie) {
		this.cookieManager = cookie;
	}

	public long getID(){
		return this.id;
	}
	
	public boolean getLogged(){
		return this.isLoggedIn;
	}
	


}
