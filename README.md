Three-module project with a proxy, a web interface and a server. The proxy has been realized modifying Zap Proxy (Java), to make it able to collect and send data about download speed and latency of the user to the server. The web interface has been realized in AngularJS and it is a Single Page Application, which allows the user to view the data collected in real time through charts and tables (Web sockets and google charts used), comparing the different performances with the user's other ISPs or with other users. The server is written in Java and based on the Spring framework. It provides data collection through RESTful web services and the Hibernate Framework, users management through Spring Security, and web sockets handling. The service has been designed to be scalable, so the majority of the data elaboration is done client-side where possible.

The project has been developed for a university assignment at Politecnico di Torino and it does not thus come with a full installation guide.

Developers:
Aloi Antonino (antonino.aloi@outlook.com)
Bruno Luca (luca.bruno.1991@gmail.com)
Monge Alberto (albertomonge91@gmail.com)
Piumatti Danilo (danilo.piumatti@gmail.com)

Disclaimer
The company team "Maddox Inc." just refers to a BitBucket team and has nothing to do with any real company.