package speedTest;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.MessageSource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import speedTest.entities.User;
import speedTest.services.UserService;

@Component
public class RegistrationListener implements ApplicationListener<OnRegistrationCompleteEvent> {
    @Autowired
    private UserService service;
    @Autowired
    private MessageSource messages;
    @Autowired
    private JavaMailSender mailSender;
 
    public void onApplicationEvent(OnRegistrationCompleteEvent event) {
        this.confirmRegistration(event);
    }
 
    private void confirmRegistration(OnRegistrationCompleteEvent event) {
        User user = event.getUser();
        //String token = UUID.randomUUID().toString();
        //service.createVerificationToken(user, token);
         
        String recipientAddress = user.getUsername();
        String subject = "Registration Confirmation";
        //String confirmationUrl = event.getAppUrl() + "/regitrationConfirm.html?token=" + token;
        //String message = messages.getMessage("message.regSucc", null, event.getLocale());
        SimpleMailMessage email = new SimpleMailMessage();
        email.setFrom("ghisupermarket@gmail.com");
        email.setTo(recipientAddress);
        email.setSubject(subject);
        email.setText("Click this link to activate your account: " + "https://" + event.getAppUrl() + "/userCheck/" + user.getId());;
        mailSender.send(email);
    }
}