package speedTest;



import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.orm.hibernate3.HibernateExceptionTranslator;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import speedTest.repositories.MeasureRepository;
import speedTest.services.CustomUserDetailsService;
import speedTest.services.MeasureService;
import speedTest.services.MeasureServiceImpl;
import speedTest.services.UserService;
import speedTest.services.UserServiceImpl;

@Configuration
@ComponentScan(basePackages={"speedTest","speedTest.services","speedTest.repositories"},
excludeFilters={
		@Filter(type=FilterType.ANNOTATION,value=EnableWebMvc.class)
})
@EnableJpaRepositories
public class RootConfig {
	
	@Bean
	public DataSource dataSource(){
		DriverManagerDataSource ds =new DriverManagerDataSource();
		ds.setDriverClassName("com.mysql.jdbc.Driver");
		ds.setUrl("jdbc:mysql://localhost:3306/speedTest");
		ds.setUsername("root");
		ds.setPassword("");
		
		return ds;
	}
	
	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource) {
		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		vendorAdapter.setDatabase(Database.MYSQL);
		//vendorAdapter.setGenerateDdl(true);
		vendorAdapter.setGenerateDdl(false);
		vendorAdapter.setShowSql(true);
		
		LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
		factory.setJpaVendorAdapter(vendorAdapter);
		factory.setPackagesToScan("speedTest.entities");
		factory.setDataSource(dataSource);
		return factory;
	}
	
	@Bean
	public PlatformTransactionManager transactionManager(EntityManagerFactory factory) {
		JpaTransactionManager txManager = new JpaTransactionManager();
		txManager.setEntityManagerFactory(factory);
		return txManager;
	}
	
	
	@Bean
	public UserService myUserService() {
		return new UserServiceImpl();
	}
	
	@Bean
	public MeasureService myMeasureService() {
		return new MeasureServiceImpl();
	}

	@Bean
	public UserDetailsService userDetailService() {
		return new CustomUserDetailsService();
	}
	
	
	/*@Bean
	public RegistrationListener myRegistrationListener(){
		return new RegistrationListener();
	}*/
	
	@Bean 
	public JavaMailSender mailSender(){
		//boolean isAuthenticated = true;
		  JavaMailSenderImpl javaMailSenderImpl=new JavaMailSenderImpl();
		  javaMailSenderImpl.setProtocol("smtp");
		  
		javaMailSenderImpl.setHost("smtp.gmail.com");
		javaMailSenderImpl.setPort(587);
	    javaMailSenderImpl.setUsername("ghisupermarket@gmail.com");
	    javaMailSenderImpl.setPassword("fezspider");
	    Properties props=new Properties();
	    props.setProperty("mail.smtp.auth","true");
	    props.setProperty("mail.smtp.starttls.enable","true");
	    props.setProperty("mail.smtp.ssl.trust","*");
	    javaMailSenderImpl.setJavaMailProperties(props);
		  
		  return javaMailSenderImpl;
	}
}
