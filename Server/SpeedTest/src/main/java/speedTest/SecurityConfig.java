package speedTest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;

import speedTest.CsrfHeaderFilter;
import speedTest.services.CustomUserDetailsService;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserDetailsService customUserDetailsService;

	
	@Override
	  protected void configure(AuthenticationManagerBuilder auth)
				throws Exception {
		//configura gli utenti e i ruoli
//		auth.
//		inMemoryAuthentication().
//			withUser("admin").
//			password("admin").
//			roles("ADMIN").
//		and().
//			withUser("user2").
//			password("pass2").
//			roles("USER");
		
		auth.userDetailsService(customUserDetailsService);

	  }
	  @Override
	  protected void configure(HttpSecurity http) 
				throws Exception {
		//configura le URL da proteggere
		  http
		  .httpBasic()
		   .and()
		   .authorizeRequests()
//		   		.antMatchers("/","/index.html").permitAll()
//		   		.antMatchers("/users/**").hasRole("ADMIN")
//		   		.and()
//		     .formLogin()
		   		.antMatchers("/index.html", "/resources/**", "/", "/measures/globalAverages/**", "/measures/providers", "/realTimeMeasures/**", "/globalRealTimeMeasures/**").permitAll()
		   		.antMatchers(HttpMethod.POST, "/users").permitAll()
		   		.antMatchers(HttpMethod.GET, "/userCheck/{id}").permitAll()
		   		.anyRequest().authenticated()
		     .and().formLogin().loginPage("/").and().logout().logoutSuccessUrl("/").and()
	          .addFilterAfter(new CsrfHeaderFilter(), CsrfFilter.class).csrf().csrfTokenRepository(csrfTokenRepository());

		  }
	  
	  private CsrfTokenRepository csrfTokenRepository() {
		  HttpSessionCsrfTokenRepository repository = new HttpSessionCsrfTokenRepository();
		  repository.setHeaderName("X-XSRF-TOKEN");
		  return repository;
		}
	  
	  
	  @Override
	  public void configure(WebSecurity web) throws Exception {
		//configura la catena dei filtri di sicurezza
	  }

	  
	  
	
}
