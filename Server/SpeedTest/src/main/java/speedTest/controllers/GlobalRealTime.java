package speedTest.controllers;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.TimerTask;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Component;

import speedTest.entities.ASAverageMeasures;
import speedTest.entities.FrequencyMeasures;
import speedTest.services.MeasureService;
import speedTest.websocket.MeasuresGlobalRealTimeClient;

public class GlobalRealTime extends TimerTask {

	@Autowired
	MeasureService measureService;
	
	MeasuresGlobalRealTimeClient client;
	private final String webSocketAddress = "ws://localhost:8080/SpeedTest/globalRealTimeMeasures/all/average";
	
	public GlobalRealTime(MeasureService measureService){
		this.measureService = measureService;
	}
	
	@Override
	public void run() {
		System.out.println("timeout!");
		long end = System.currentTimeMillis();
		long start = end - (end % 86400000);
		List<ASAverageMeasures> avgMeasures;
		List<FrequencyMeasures> freqMeasures;

		if (client == null) {
			try {
				client = new MeasuresGlobalRealTimeClient( new URI(webSocketAddress));
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}
		}
		
		List<String> providers = measureService.getProviders();
		for(String provider : providers){
			avgMeasures = measureService.getMeasureAverages(provider,start,end);
			freqMeasures = measureService.findFrequencies(provider);
			
			if(avgMeasures.size()>0){
				client.sendMessage(avgMeasures);
			}
			
			if(freqMeasures.size()>0){
				client.sendFrequencyMessage(freqMeasures);
			}
		}
		
		/*avgMeasures = measureService.getMeasureAverages(start,end);
		freqMeasures = measureService.findFrequencies();

		if(avgMeasures.size()>0){
			client.sendMessage(avgMeasures);
		}
		
		if(freqMeasures.size()>0){
			client.sendFrequencyMessage(freqMeasures);
		}*/

	}

}
