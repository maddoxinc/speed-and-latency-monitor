package speedTest.controllers;

import java.security.Principal;

import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import speedTest.entities.User;

@RestController
public class LoginController {
	
	 @RequestMapping(value = "/user", method = RequestMethod.GET)
	  public Principal user(Principal user) {
	  	return user;
	  }
}
