package speedTest.controllers;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import speedTest.entities.AverageMeasures;
import speedTest.entities.DomainMeasure;
import speedTest.entities.DomainSizeMeasure;
import speedTest.entities.FrequencyMeasures;
import speedTest.entities.LatencyMeasures;
import speedTest.entities.Measure;
import speedTest.entities.MeasurePage;
import speedTest.services.MeasureService;
import speedTest.websocket.MeasuresRealTimeClient;

@RestController
public class MeasureController {
	private MeasuresRealTimeClient client;
	private final String webSocketAddress = "ws://localhost:8080/SpeedTest/realTimeMeasures";
	
	private void initializeWebSocket() throws URISyntaxException {
		System.out.println("REST service: open websocket client at " + webSocketAddress);
		client = new MeasuresRealTimeClient( new URI(webSocketAddress + "/-1" + "/" + false));
	}
	
	private void sendMessageOverSocket(List<Measure> measure) {
		if (client == null) {
			try {
				initializeWebSocket();
				System.out.println("initialize ok");
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}
		}
		
		client.sendMessage(measure);
	}
	
	@Autowired
	MeasureService measureService;
	
	@RequestMapping(value = "/measures",method=RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.CREATED)
	public void addMeasures(@RequestBody List<Measure> measures){
		try{
			measureService.addMeasures(measures);
			sendMessageOverSocket(measures);
		}
		catch(Throwable t){
			System.out.println(t.getMessage());
		}
	}
	
	@RequestMapping(value = "/measures/{id}/{index}/pagesize/{pagesize}",method=RequestMethod.GET)
	public Page<Measure> getUserMeasures(@PathVariable("id") Long id, @PathVariable("index") int index, @PathVariable("pagesize") int pageSize){
		try{
			System.out.println("index:"+index+" Pagesize:"+ pageSize);
			MeasurePage mp = new MeasurePage(index, pageSize);
			return measureService.findAll(mp);
			//return measureService.getUserMeasures(id);
		}
		catch(Throwable t){
			return null;
		}
	
	}
	
	@RequestMapping(value = "/measures/userAverages/{asnum}/{id}/{start}/{end}",method=RequestMethod.GET)
	public List<AverageMeasures> getUserFilteredMeasures(@PathVariable("asnum") String asnum,@PathVariable("id") Long id,@PathVariable("start") Long start,@PathVariable("end") Long end){
		try{
			System.out.println("CONTROLLER ASNUM: " + asnum);
			return measureService.getMeasureAverages(id,start,end,asnum);
		}
		catch(Throwable t){
			return null;
		}
	
	}
	
	@RequestMapping(value = "/measures/globalAverages/{asnum}/{start}/{end}",method=RequestMethod.GET)
	public List<AverageMeasures> getGlobalFilteredMeasures(@PathVariable("asnum") String asnum,@PathVariable("start") Long start,@PathVariable("end") Long end){
		try{
			return measureService.getMeasureAverages(start,end,asnum);
		}
		catch(Throwable t){
			return null;
		}
	
	}
	
	@RequestMapping(value = "/measures/userFrequencies/count/{asnum}/{id}",method=RequestMethod.GET)
	public Long getMeasureFrequenciesCount(@PathVariable("asnum") String asnum,@PathVariable("id") Long id){
		try{
			Long count = measureService.countFromUserAndProvider(id, asnum);
			return count;
		}
		catch(Throwable t){
			return null;
		}
	}
	
	@RequestMapping(value = "/measures/globalFrequencies/count/{asnum}",method=RequestMethod.GET)
	public Long getGlobalMeasureFrequenciesCount(@PathVariable("asnum") String asnum){
		try{
			Long count = measureService.countFromProvider(asnum);
			return count;
		}
		catch(Throwable t){
			return null;
		}
	}
	
	@RequestMapping(value = "/measures/userFrequencies/{asnum}/{id}/{start}/{end}",method=RequestMethod.GET)
	public List<FrequencyMeasures> getMeasureFrequencies(@PathVariable("asnum") String asnum,@PathVariable("id") Long id,@PathVariable("start") int start,@PathVariable("end") int end){
		try{
			return measureService.findFrequencies(id, asnum,start,end);
		}
		catch(Throwable t){
			return null;
		}
	}
	
	@RequestMapping(value = "/measures/globalFrequencies/{asnum}/{id}/{start}/{end}",method=RequestMethod.GET)
	public List<FrequencyMeasures> getGlobalMeasureFrequencies(@PathVariable("asnum") String asnum,@PathVariable("start") int start,@PathVariable("end") int end){
		try{
			return measureService.findFrequencies(asnum,start,end);
		}
		catch(Throwable t){
			return null;
		}
	}
	
	@RequestMapping(value = "/measures/userLatencies/{asnum}/{id}/{interval}",method=RequestMethod.GET)
	public List<LatencyMeasures> getMeasureLatencies(@PathVariable("asnum") String asnum,@PathVariable("id") Long id,@PathVariable("interval") int interval){
		try{
			return measureService.findLatencies(id, asnum,interval);
		}
		catch(Throwable t){
			System.out.println(t + " " + t.getMessage() + " " + t.getCause());
			return null;
		}
	}
	
	
	@RequestMapping(value = "/measures/providers",method=RequestMethod.GET)
	public List<String> getProviders(){
		try{
			return measureService.getProviders();
		}
		catch(Throwable t){
			return null;
		}
	}
	
	@RequestMapping(value = "/measures/providers/{id}",method=RequestMethod.GET)
	public List<String> getProviders(@PathVariable("id") Long id){
		try{
			return measureService.getProviders(id);
		}
		catch(Throwable t){
			return null;
		}
	}
	
	@RequestMapping(value = "/measures/domain/{id}",method=RequestMethod.GET)
	public List<DomainMeasure> getDomainMeasures(@PathVariable("id") Long id){
		try{
			return measureService.getDomainMeasures(id);
		}
		catch(Throwable t){
			return null;
		}
	}
	
	@RequestMapping(value = "/measures/domainSize/{id}",method=RequestMethod.GET)
	public List<DomainSizeMeasure> getDomainSizeMeasures(@PathVariable("id") Long id){
		try{
			return measureService.getDomainSizeMeasures(id);
		}
		catch(Throwable t){
			return null;
		}
	}
	
	
	
}
