package speedTest.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PublicIPController {

	
	@RequestMapping(value = "/publicip", method=RequestMethod.GET)
	public String publicIp(HttpServletRequest req){
		
		return req.getRemoteAddr();
	
	}
}
