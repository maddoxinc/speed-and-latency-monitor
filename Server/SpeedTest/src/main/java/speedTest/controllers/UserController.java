package speedTest.controllers;


import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import speedTest.OnRegistrationCompleteEvent;
import speedTest.entities.User;
import speedTest.entities.UserCheckException;
import speedTest.services.UserService;

@RestController
public class UserController {
	
	
	@Autowired
	UserService userService;
	@Autowired
	ApplicationEventPublisher eventPublisher;
	
	@ExceptionHandler(org.springframework.dao.DataIntegrityViolationException.class)
	public String handleSQLException(){ 
		return "{ \"resp\" : \"duplicate\" }"; 
	}
	  
	@ExceptionHandler(IOException.class)
	public String handleIOException(IOException ex) {
		return "{ \"resp\" : \"IOException\" }"; 
	}
	
	@ExceptionHandler(UserCheckException.class)
	public String handleUserCheckException(UserCheckException ex) {
		return "{ \"resp\" : \"UserCheckException\" }"; 
	}
	
	
	@RequestMapping(value="/users/{id}", method=RequestMethod.GET)
	public User getUserById(@PathVariable("id") Long Id){
		try{
			System.out.println("user controller");
			return userService.getUserById(Id);
		}
		catch(Throwable t){
			System.out.println(t + " " + t.getMessage() + " " + t.getCause());
			return null;
		}
	}
	
	@RequestMapping(value="/users",method=RequestMethod.POST)
	public String addUser(@RequestBody User u, HttpServletRequest request, HttpServletResponse resp) throws IOException, SQLException {
		String address = request.getLocalName() + ":" + request.getLocalPort() + request.getContextPath();
		userService.addUser(u);
		System.out.println(u.getId());
		eventPublisher.publishEvent(new OnRegistrationCompleteEvent(u, request.getLocale(), address));

		return "{ \"resp\" : \"success\" }";
	}
	
	@RequestMapping(value="/userCheck/{id}", method=RequestMethod.GET)
	public void enableUser(@PathVariable("id") Long Id, HttpServletResponse resp) throws IOException, SQLException, UserCheckException {
		userService.enableUser(Id);
		//TODO: Aggiungere avviso registrazione confermata con successo
		resp.sendRedirect("/SpeedTest/index.html#/home/1");
	}
}
