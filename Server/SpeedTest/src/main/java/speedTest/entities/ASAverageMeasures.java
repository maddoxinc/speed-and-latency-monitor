package speedTest.entities;

public class ASAverageMeasures {
	private Long timestamp;
	private String ASNum;
	private Long sum;
	private Long totalMeasures;
	
	public ASAverageMeasures(Long timestamp, String ASNum, Long sum, Long totalMeasures) {
		this.timestamp = timestamp;
		this.ASNum = ASNum;
		this.sum = sum;
		this.totalMeasures = totalMeasures;
	}
	
	public Long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}
	public String getASNum() {
		return ASNum;
	}
	public void setASNum(String aSNum) {
		ASNum = aSNum;
	}
	public Long getSum() {
		return sum;
	}
	public void setSum(Long sum) {
		this.sum = sum;
	}
	public Long getTotalMeasures() {
		return totalMeasures;
	}
	public void setTotalMeasures(Long totalMeasures) {
		this.totalMeasures = totalMeasures;
	}
	
	
}
