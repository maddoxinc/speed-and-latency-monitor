package speedTest.entities;

public class AverageMeasures {
	private Long timestamp;
	private Long sum;
	private Long totalMeasures;
	
	public AverageMeasures(Long timestamp, Long sum, Long totalMeasures) {
		this.timestamp = timestamp;
		this.sum = sum;
		this.totalMeasures = totalMeasures;
	}
	
	public Long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}
	public Long getSum() {
		return sum;
	}
	public void setSum(Long sum) {
		this.sum = sum;
	}
	public Long getTotalMeasures() {
		return totalMeasures;
	}
	public void setTotalMeasures(Long totalMeasures) {
		this.totalMeasures = totalMeasures;
	}
	
	
}
