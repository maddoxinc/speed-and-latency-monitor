package speedTest.entities;

public class DomainMeasure {
	
	private String serverDomain;
	private long frequency;
	
	
	
	public DomainMeasure(String serverDomain, long frequency) {
		super();
		this.serverDomain = serverDomain;
		this.frequency = frequency;
	}
	public String getServerDomain() {
		return serverDomain;
	}
	public void setServerDomain(String serverDomain) {
		this.serverDomain = serverDomain;
	}
	public long getFrequency() {
		return frequency;
	}
	public void setFrequence(long frequency) {
		this.frequency = frequency;
	}
	
	

}
