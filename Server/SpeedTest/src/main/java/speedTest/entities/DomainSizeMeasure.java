package speedTest.entities;

public class DomainSizeMeasure {
	private String serverDomain;
	private Long totalDownloadSize;
	
	
	public DomainSizeMeasure(String serverDomain, Long totalDownloadSize) {
		super();
		this.serverDomain = serverDomain;
		this.totalDownloadSize = totalDownloadSize;
	}
	
	public String getServerDomain() {
		return serverDomain;
	}
	public void setServerDomain(String serverDomain) {
		this.serverDomain = serverDomain;
	}
	public Long getTotalDownloadSize() {
		return totalDownloadSize;
	}
	public void setTotalDownloadSize(Long totalDownloadSize) {
		this.totalDownloadSize = totalDownloadSize;
	}
	
}
