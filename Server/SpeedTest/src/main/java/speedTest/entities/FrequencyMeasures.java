package speedTest.entities;

public class FrequencyMeasures {
	private int speed;
	private Long frequency;
	private Long total;
	private String ASNum;
	
	public FrequencyMeasures(int speed, Long frequency,String ASNum){
		this.speed = speed;
		this.frequency = frequency;
		this.ASNum = ASNum;
	}
	
	public int getSpeed() {
		return speed;
	}
	public void setSpeed(int speed) {
		this.speed = speed;
	}
	public Long getFrequency() {
		return frequency;
	}
	public void setFrequency(long frequency) {
		this.frequency = frequency;
	}

	public Long getTotal() {
		return total;
	}

	public void setTotal(Long total) {
		this.total = total;
	}

	public String getASNum() {
		return ASNum;
	}

	public void setASNum(String aSNum) {
		ASNum = aSNum;
	}

	
	
}
