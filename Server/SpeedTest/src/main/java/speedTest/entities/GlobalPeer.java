package speedTest.entities;

import javax.websocket.Session;

public class GlobalPeer {

	private javax.websocket.Session session;
	private String asnum;
	private String id;
	private String type;
	
	public GlobalPeer(Session session, String asnum,String type) {
		super();
		this.session = session;
		this.asnum = asnum;
		this.id = session.getId();
		this.setType(type);
	}

	public javax.websocket.Session getSession() {
		return session;
	}

	public void setSession(javax.websocket.Session session) {
		this.session = session;
	}

	public String getAsnum() {
		return asnum;
	}

	public void setAsnum(String asnum) {
		this.asnum = asnum;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((asnum == null) ? 0 : asnum.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GlobalPeer other = (GlobalPeer) obj;
		if (asnum == null) {
			if (other.asnum != null)
				return false;
		} else if (!asnum.equals(other.asnum))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	
}
