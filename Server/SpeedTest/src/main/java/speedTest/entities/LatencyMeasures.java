package speedTest.entities;

public class LatencyMeasures {
	private String latencyInterval;
	private Long frequency;
	private Long total;
	private String ASNum;
	public LatencyMeasures(String latencyInterval, Long frequency, Long total,
			String aSNum) {
		super();
		this.latencyInterval = latencyInterval;
		this.frequency = frequency;
		this.total = total;
		ASNum = aSNum;
	}
	public String getLatencyInterval() {
		return latencyInterval;
	}
	public void setLatencyInterval(String latencyInterval) {
		this.latencyInterval = latencyInterval;
	}
	public Long getFrequency() {
		return frequency;
	}
	public void setFrequency(Long frequency) {
		this.frequency = frequency;
	}
	public Long getTotal() {
		return total;
	}
	public void setTotal(Long total) {
		this.total = total;
	}
	public String getASNum() {
		return ASNum;
	}
	public void setASNum(String aSNum) {
		ASNum = aSNum;
	}
	
}
