package speedTest.entities;

import javax.websocket.Session;

public class Peer {

	private javax.websocket.Session session;
	private String id;
	private boolean speedFlag;
	
	public Peer(Session session, String id, boolean speedFlag) {
		super();
		this.session = session;
		this.id = id;
		this.speedFlag = speedFlag;
	}

	
	public boolean isSpeedFlag() {
		return speedFlag;
	}



	public void setSpeedFlag(boolean speedFlag) {
		this.speedFlag = speedFlag;
	}



	public javax.websocket.Session getSession() {
		return session;
	}

	public void setSession(javax.websocket.Session session) {
		this.session = session;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Peer other = (Peer) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
	
}
