package speedTest.entities;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class SecurityUser extends User implements UserDetails
{

	private static final long serialVersionUID = 1L;
	public SecurityUser(User user) {
		if(user != null)
		{
			this.setId(user.getId());
			this.setName(user.getName());
			this.setUsername(user.getUsername());
			this.setPassword(user.getPassword());
			this.setSurname(user.getSurname());
			this.setRoles(user.getRoles());
			this.setEnabled(user.isEnabled());
		}		
	}
	
	
	public Collection<? extends GrantedAuthority> getAuthorities() {
		
		Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		Set<Role> userRoles = this.getRoles();
		
		
		if(userRoles != null)
		{
			for (Role role : userRoles) {
				SimpleGrantedAuthority authority = new SimpleGrantedAuthority(role.getRole());
				authorities.add(authority);
			}
			
		}
		return authorities;
	}

	@Override
	public String getPassword() {
		return super.getPassword();
	}

	@Override
	public String getUsername() {
		return super.getUsername();
	}

	
	public boolean isAccountNonExpired() {
		return super.isEnabled();
	}


	public boolean isAccountNonLocked() {
		return super.isEnabled();
	}


	public boolean isCredentialsNonExpired() {
		return super.isEnabled();
	}


	public boolean isEnabled() {
		return super.isEnabled();
	}	
}
