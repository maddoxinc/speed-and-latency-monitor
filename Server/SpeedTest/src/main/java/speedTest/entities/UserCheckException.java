package speedTest.entities;

@SuppressWarnings("serial")
public class UserCheckException extends Exception {
	public UserCheckException() {
		super();
	}
	
	public UserCheckException(String message) {
		super(message);
	}
}
