package speedTest.repositories;

import java.util.Collection;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import speedTest.entities.ASAverageMeasures;
import speedTest.entities.AverageMeasures;
import speedTest.entities.DomainMeasure;
import speedTest.entities.DomainSizeMeasure;
import speedTest.entities.FrequencyMeasures;
import speedTest.entities.Measure;

@Repository
public interface MeasureRepository extends PagingAndSortingRepository<Measure, Long>{

	  @Query("select count(*) from Measure m where m.uuid = ?1 and m.asnum = ?2 and m.downloadSpeed > 0")
	Long countDownloadSpeedFromUserAndProvider(Long id, String provider);
	  
	  @Query("select count(*) from Measure m where m.asnum = ?1 and m.downloadSpeed > 0")
	Long countDownloadSpeedFromProvider(String provider);
	  
	  @Query("select count(*) from Measure m where m.uuid = ?1 and m.asnum = ?2 and m.connectTime >= 0")
	Long countConnectTimeFromUserAndProvider(Long id,String provider);
	  
	  @Query("select m from Measure m where m.uuid = ?1 and m.timestamp >= ?2 and m.timestamp <= ?3 order by m.timestamp asc")
	List<AverageMeasures> findFromTo(long id,long start, long end);
	
	  @Query("select new speedTest.entities.AverageMeasures(m.date,sum(m.downloadSpeed),count(*))  from Measure m where m.uuid = ?1 and m.timestamp >= ?2 and m.timestamp <= ?3 and m.asnum = ?4 and m.downloadSpeed > 0 group by m.date order by m.timestamp asc")
	List<AverageMeasures> findAveragesFromTo(long id,long start, long end,String asnum);
	  
	  @Query("select new speedTest.entities.AverageMeasures(m.date,sum(m.downloadSpeed), count(*)) from Measure m where m.timestamp >= ?1 and m.timestamp <= ?2 and m.asnum = ?3 and m.downloadSpeed > 0 group by m.date order by m.timestamp asc")
	List<AverageMeasures> findAveragesFromTo(long start, long end,String asnum);
	  
	  @Query("select new speedTest.entities.ASAverageMeasures(m.date, m.asnum, sum(m.downloadSpeed), count(*)) from Measure m where m.asnum = ?1 and m.timestamp >= ?2 and m.timestamp <= ?3 and m.downloadSpeed > 0 group by m.date, m.asnum order by m.timestamp asc")
	List<ASAverageMeasures> findAveragesFromTo(String asnum,long start, long end);
	  
	  @Query("select m from Measure m where m.uuid = ?1")
	List<Measure> FindByUuid(long id);

	  @Query("select distinct m.asnum from Measure m")
	List<String> findProviders();
	  
	  @Query("select distinct m.asnum from Measure m where m.uuid = ?1")
	List<String> findProviders(Long id);

	  @Query("select new speedTest.entities.DomainMeasure(m.serverDomain,count(*) as frequency)  from Measure m where m.uuid = ?1 group by m.serverDomain order by frequency ")
	List<DomainMeasure> findDomainMeasureFromTo(Long id);

	  @Query("select new speedTest.entities.DomainSizeMeasure(m.serverDomain,sum(m.size) as size) from Measure m where m.uuid = ?1 group by m.serverDomain order by size")
	List<DomainSizeMeasure> findDomainSizeMeasure(Long id);
	  
	Page<Measure> findAll(Pageable pageable);
	
	  @Query("select new speedTest.entities.FrequencyMeasures(m.downloadSpeed,count(*),m.asnum) from Measure m where m.uuid = ?1 and m.asnum = ?2 and m.downloadSpeed >= ?3 and m.downloadSpeed <= ?4  group by m.downloadSpeed order by m.downloadSpeed asc ")
	List<FrequencyMeasures> findFrequencies(long id, String asnum,int start,int end);
	  
	  @Query("select new speedTest.entities.FrequencyMeasures(m.downloadSpeed,count(*),m.asnum) from Measure m where m.asnum = ?1 and m.downloadSpeed >= ?2 and m.downloadSpeed <= ?3  group by m.downloadSpeed order by m.downloadSpeed asc ")
	List<FrequencyMeasures> findFrequencies(String anum,int start,int end);

	  @Query("select new speedTest.entities.FrequencyMeasures(m.downloadSpeed,count(*),m.asnum) from Measure m where m.asnum = ?1 group by m.downloadSpeed order by m.downloadSpeed asc ")
	List<FrequencyMeasures> findFrequencies(String asnum);
	 
	  @Query("select count(*) from Measure m where m.uuid = ?1 and m.asnum = ?2 and m.connectTime >= ?3 and m.connectTime <= (?3 + 49)")
	Long getLatencyCount(Long id, String asnum, Long index);

	
}
