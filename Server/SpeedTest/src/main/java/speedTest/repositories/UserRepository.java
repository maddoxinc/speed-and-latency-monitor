package speedTest.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import speedTest.entities.User;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User, Long>{
	User findByUsername(String username);
}
