package speedTest.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import speedTest.entities.ASAverageMeasures;
import speedTest.entities.AverageMeasures;
import speedTest.entities.DomainMeasure;
import speedTest.entities.DomainSizeMeasure;
import speedTest.entities.FrequencyMeasures;
import speedTest.entities.LatencyMeasures;
import speedTest.entities.Measure;
import speedTest.entities.MeasurePage;

@Service
public interface MeasureService {
	public int getIndex();
	public void addMeasures(List<Measure> measures);
	public List<Measure> getUserMeasures(long id);
	public List<AverageMeasures> getMeasures(Long id, Long start, Long end);
	public List<AverageMeasures> getMeasureAverages(Long id, Long start, Long end,String asnum);
	public List<AverageMeasures> getMeasureAverages(Long start, Long end,String asnum);
	public List<ASAverageMeasures> getMeasureAverages(String asnum,Long start, Long end);
	public List<String> getProviders();
	public List<String> getProviders(Long id);
	public List<DomainMeasure> getDomainMeasures(Long id);
	public List<DomainSizeMeasure> getDomainSizeMeasures(Long id);
	public Page<Measure> findAll(MeasurePage mp);
	public Long countFromUserAndProvider(Long id,String asnum);
	public List<FrequencyMeasures> findFrequencies(long id, String asnum,int start, int end);
	public Long countFromProvider(String asnum);
	public List<FrequencyMeasures> findFrequencies(String asnum,int start, int end);
	public List<FrequencyMeasures> findFrequencies(String asnum);
	public List<LatencyMeasures> findLatencies(Long id, String asnum,int interval);
}
