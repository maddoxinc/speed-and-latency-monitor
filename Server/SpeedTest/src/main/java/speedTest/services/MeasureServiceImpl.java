package speedTest.services;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;
import java.util.Timer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Transactional;

import speedTest.controllers.GlobalRealTime;
import speedTest.entities.ASAverageMeasures;
import speedTest.entities.AverageMeasures;
import speedTest.entities.DomainMeasure;
import speedTest.entities.DomainSizeMeasure;
import speedTest.entities.FrequencyMeasures;
import speedTest.entities.LatencyMeasures;
import speedTest.entities.Measure;
import speedTest.entities.MeasurePage;
import speedTest.repositories.MeasureRepository;

public class MeasureServiceImpl implements MeasureService {

	@Autowired
	MeasureRepository repo;

	Timer globalRealtime;
	
	public MeasureServiceImpl(){
		globalRealtime = new Timer();
        globalRealtime.scheduleAtFixedRate(new GlobalRealTime(this),15000L, 30000L);
	}
	
	
	private int indexMeasureTable=0;
	
	public void addMeasures(List<Measure> measures){
//		GregorianCalendar d = new GregorianCalendar();
//		d.setTime(new Date());
//		d.set(Calendar.HOUR, 0);
//		d.set(Calendar.MINUTE,0);
//		d.set(Calendar.SECOND, 0);
//		d.set(Calendar.MILLISECOND, 0);
		for(Measure m : measures){
			Long timestamp = m.getTimestamp();
			m.setDate(timestamp - (timestamp % 86400000));
			//m.setDate(d.getTime().getTime());
			repo.save(m);
		}
	}
	
	public int getIndex(){
		return this.indexMeasureTable;
	}
	
	public List<Measure> getUserMeasures(long id) {
		return repo.FindByUuid(id);
	}
	
	public List<AverageMeasures> getMeasures(Long id, Long start, Long end){
		List<AverageMeasures> l = repo.findFromTo(id,start, end);
		return l;
	}
	public List<AverageMeasures> getMeasureAverages(Long id, Long start, Long end, String asnum){
		List<AverageMeasures> l = repo.findAveragesFromTo(id,start, end,asnum);
		return l;
	}
	
	public List<AverageMeasures> getMeasureAverages(Long start, Long end,String asnum){
		return repo.findAveragesFromTo(start, end,asnum);
	}
	
	public List<ASAverageMeasures> getMeasureAverages(String asnum,Long start, Long end){
		return repo.findAveragesFromTo(asnum,start, end);
	}

	public List<String> getProviders() {
		return repo.findProviders();
	}
	
	public List<String> getProviders(Long id) {
		return repo.findProviders(id);
	}

	public List<DomainMeasure> getDomainMeasures(Long id) {
		return repo.findDomainMeasureFromTo(id);
	}
	
	public List<DomainSizeMeasure> getDomainSizeMeasures(Long id) {
		return repo.findDomainSizeMeasure(id);
	}

	public Page<Measure> findAll(MeasurePage mp) {
		return repo.findAll(mp);
	}

	public Long countFromUserAndProvider(Long id, String asnum) {
		return repo.countDownloadSpeedFromUserAndProvider(id, asnum);
	}

	@Transactional
	public List<FrequencyMeasures> findFrequencies(long id, String asnum,int start, int end) {
		Long total = repo.countDownloadSpeedFromUserAndProvider(id, asnum);
		List<FrequencyMeasures> frequencies = repo.findFrequencies(id,asnum, start, end);
		for(FrequencyMeasures frequency : frequencies){
			frequency.setTotal(total);
		}
		return frequencies;
	}

	public Long countFromProvider(String asnum) {
		return repo.countDownloadSpeedFromProvider(asnum);
	}

	@Transactional
	public List<FrequencyMeasures> findFrequencies(String asnum, int start,
			int end) {
		Long total = repo.countDownloadSpeedFromProvider(asnum);
		List<FrequencyMeasures> frequencies = repo.findFrequencies(asnum, start, end);
		for(FrequencyMeasures frequency : frequencies){
			frequency.setTotal(total);
		}
		return frequencies;
	}

	@Transactional
	public List<FrequencyMeasures> findFrequencies(String asnum) {
		List<FrequencyMeasures> frequencies = new LinkedList<FrequencyMeasures>();
		Long total = repo.countDownloadSpeedFromProvider(asnum);
		frequencies.addAll(repo.findFrequencies(asnum));
		for(FrequencyMeasures frequency : frequencies){
			frequency.setTotal(total);
		}
		
		return frequencies;
	}

	@Transactional
	public List<LatencyMeasures> findLatencies(Long id, String asnum,int interval) {
		int start;
		int end;
		switch(interval){
			case 0:
				start = 0;
				end = 1000;
				break;
			case 1:
				start = 1000;
				end = 2000;
				break;
			case 2:
				start = 2000;
				end = 3000;
				break;
			case 3:
				start = 3000;
				end = 4000;
				break;
			case 4:
				start = 4000;
				end = 5000;
				break;
			default:
				start = 0;
				end = 1000;
		}
		
		int index;
		Long total = repo.countConnectTimeFromUserAndProvider(id,asnum);
		List<LatencyMeasures> latencies = new LinkedList<LatencyMeasures>();
		for(index = start; index <= end; index += 50){
			LatencyMeasures m = new LatencyMeasures(index + "-" + (index + 49),repo.getLatencyCount(id,asnum,Long.valueOf(index)),total,asnum);
			latencies.add(m);
		}
		return latencies;
	}

	
}
