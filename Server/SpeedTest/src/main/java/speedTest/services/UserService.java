package speedTest.services;

import org.springframework.stereotype.Service;

import speedTest.entities.User;
import speedTest.entities.UserCheckException;

@Service
public interface UserService {

	public User getUserById(Long id);
	public User getUserByUsername(String username);
	public void addUser(User u);
	public void enableUser(Long id) throws UserCheckException;
}
