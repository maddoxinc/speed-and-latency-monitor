package speedTest.services;

import org.springframework.beans.factory.annotation.Autowired;

import speedTest.entities.User;
import speedTest.entities.UserCheckException;
import speedTest.repositories.UserRepository;

public class UserServiceImpl implements UserService {

	@Autowired
	UserRepository repo;

	public User getUserById(Long id) {
		return repo.findOne(id);
	}
	
	public User getUserByUsername(String username) {
		User u = repo.findByUsername(username); 
		return u;
	}
	
	public void addUser(User user){
		repo.save(user);
	}
	
	public void enableUser(Long id) throws UserCheckException {
		User u = repo.findOne(id);
		if (u == null) {
			throw new UserCheckException("User not found");
		}
		if (u.isEnabled()) {
			throw new UserCheckException("User already enabled");
		}
		u.setEnabled(true);
		repo.save(u);
	}
	
	
}
