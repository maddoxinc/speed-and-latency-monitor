package speedTest.websocket;

import java.net.URI;
import java.util.List;

import javax.websocket.ClientEndpoint;
import javax.websocket.CloseReason;
import javax.websocket.ContainerProvider;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.WebSocketContainer;

import speedTest.entities.ASAverageMeasures;
import speedTest.entities.FrequencyMeasures;

import com.google.gson.Gson;


@ClientEndpoint
public class MeasuresGlobalRealTimeClient {
	
	public MeasuresGlobalRealTimeClient(URI endpointURI) {
		try {
			WebSocketContainer container = ContainerProvider.getWebSocketContainer();
			container.setDefaultMaxTextMessageBufferSize(32768);
			container.setDefaultMaxBinaryMessageBufferSize(32768);
			container.connectToServer(this, endpointURI);
			} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new RuntimeException(e);
		}
	}
	
	Session userSession = null;
	
	@OnOpen
	public void onOpen(Session userSession) {
		System.out.println("client: opening websocket ");
		this.userSession = userSession;
	}
	
	/**
	* Callback hook for Connection close events.
	*
	* @param userSession the userSession which is getting closed.
	* @param reason the reason for connection close
	*/
	@OnClose
	public void onClose(Session userSession, CloseReason reason) {
		System.out.println("client: closing websocket");
		this.userSession = null;
	}
	
	/**
	* Callback hook for Message Events. This method will be invoked when a client send a message.
	*
	* @param message The text message
	*/
	@OnMessage
	public void onMessage(String message) {
		System.out.println("client: received message: " + message);
	}
	 
	public void sendMessage(List<ASAverageMeasures> message) {
		Gson gson = new Gson();
		String json = gson.toJson(message);

		System.out.println("client: send global average message " + json);
		
		try{
			this.userSession.getAsyncRemote().sendText(json);
		}catch(Exception e){
			System.out.println(e.getMessage());
		} 
	}
	
	public void sendFrequencyMessage(List<FrequencyMeasures> message){
		Gson gson = new Gson();
		String json = gson.toJson(message);

		System.out.println("client: send global frequency message " + json);
		
		try{
			this.userSession.getAsyncRemote().sendText(json);
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}
	 
}
