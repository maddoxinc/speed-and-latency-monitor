package speedTest.websocket;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.websocket.CloseReason;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

import speedTest.entities.ASAverageMeasures;
import speedTest.entities.FrequencyMeasures;
import speedTest.entities.GlobalPeer;

import com.google.gson.Gson;



@ServerEndpoint("/globalRealTimeMeasures/{asnum}/{type}")
public class MeasuresGlobalRealTimeMonitor {
	private static Set<GlobalPeer> peers = Collections.synchronizedSet(new HashSet<GlobalPeer>());
	
	
	@OnOpen
	public void onOpen(Session session, @PathParam("asnum") String asnum,@PathParam("type") String type) {
		System.out.println("mediator: opened websocket channel for client " + asnum);
		//remove the peer if already present
		//peers.remove(new GlobalPeer(session, asnum));
		peers.add(new GlobalPeer(session, asnum,type));
	}
	
	@OnMessage
	public String onMessage(String message, Session session) {
		try {
			//System.out.println("Message received from web socket server ");
			Gson gson = new Gson();
			try{
				FrequencyMeasures[] mf = gson.fromJson(message, FrequencyMeasures[].class);
				if (mf[0].getFrequency() == null) { //Average
					ASAverageMeasures[] m = gson.fromJson(message, ASAverageMeasures[].class);
					//Map<String, ASAverageMeasures> map = new HashMap<String, ASAverageMeasures>();
					/*for (ASAverageMeasures am : m) {
						map.put(am.getASNum(), am);
					}*/
					
					System.out.println("received message from client ");
					for (GlobalPeer s : peers) {
						if(s.getAsnum().compareTo("all")!=0 && s.getAsnum().equals(m[0].getASNum())  && s.getType().equals("average")){
							
							//String json = gson.toJson(m);
							s.getSession().getAsyncRemote().sendText(message);
							System.out.println("send download speed to peer - average");
						}
					}
				}
				else { //Frequency
					/*Map<String, FrequencyMeasures> mapf = new HashMap<String, FrequencyMeasures>();
					for (FrequencyMeasures fm : mf) {
						mapf.put(fm.getASNum(), fm);
					}*/
					
					System.out.println("received message from client ");
					for (GlobalPeer s : peers) {
						if(s.getAsnum().compareTo("all")!=0 && s.getAsnum().equals(mf[0].getASNum()) && s.getType().equals("frequency")){
							//String json = gson.toJson(mf);
							s.getSession().getAsyncRemote().sendText(message);
							System.out.println("send download speed to peer - frequency");
						}
					}
				}
			}catch(Throwable t){
				t.printStackTrace();
			}
				
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "message was received by socket mediator and processed: " + message;
	}
	
	
	
	
	@OnClose
	public void onClose(Session session, CloseReason reason, @PathParam("asnum") String asnum,@PathParam("type") String type) {
		System.out.println("mediator: closed websocket channel for client " + reason.getReasonPhrase() +asnum);
		peers.remove(new GlobalPeer(session, asnum,type));
	}
	
	@OnError
	public void onError(Session session, Throwable t, @PathParam("asnum") String asnum,@PathParam("type") String type) {
		System.out.println("mediator: closed websocket channel for client " + t.getMessage() + t.getCause() +asnum);
		peers.remove(new GlobalPeer(session, asnum,type));
	}




	
	
	
}
