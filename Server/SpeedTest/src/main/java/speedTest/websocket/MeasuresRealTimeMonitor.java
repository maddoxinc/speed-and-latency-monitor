package speedTest.websocket;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.websocket.CloseReason;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

import speedTest.entities.Measure;
import speedTest.entities.Peer;

import com.google.gson.Gson;



@ServerEndpoint("/realTimeMeasures/{Uuid}/{speedFlag}")
public class MeasuresRealTimeMonitor {
	private static Set<Peer> peers = Collections.synchronizedSet(new HashSet<Peer>());
	
	
	@OnOpen
	public void onOpen(Session session, @PathParam("Uuid") String clientId, @PathParam("speedFlag") boolean speedFlag) {
		System.out.println("mediator: opened websocket channel for client " + clientId);
		//remove the peer if already present
		peers.remove(new Peer(session, clientId, speedFlag));
		peers.add(new Peer(session, clientId, speedFlag));
		
	}
	
	@OnMessage
	public String onMessage(String message, Session session) {
		try {
			//System.out.println("Message received from web socket server ");
			Gson gson = new Gson();
			Measure[] m = gson.fromJson(message, Measure[].class);
			
				System.out.println("received message from client " + m[0].getUuid());
				for (Peer s : peers) {
					if(s.getId().compareTo(m[0].getUuid().toString())==0){
						
						if(s.isSpeedFlag()){
							List<Measure> sendList = new ArrayList<Measure>();
							for(Measure measure : m){
								if(measure.getDownloadSpeed()>0){
									sendList.add(measure);
								}
							}
							if(sendList.size()>0){
								String json = gson.toJson(sendList);
								s.getSession().getAsyncRemote().sendText(json);
								System.out.println("send message without speed=0 to peer ");
							}	
						}else{
							s.getSession().getAsyncRemote().sendText(message);
							System.out.println("send message to peer ");
						}
					}
				}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "message was received by socket mediator and processed: " + message;
	}
	
	
	
	
	@OnClose
	public void onClose(Session session, CloseReason reason, @PathParam("Uuid") String clientId, @PathParam("speedFlag") boolean speedFlag) {
		System.out.println("mediator: closed websocket channel for client " + reason.getReasonPhrase() +clientId);
		peers.remove(new Peer(session, clientId, speedFlag));
	}
	
	@OnError
	public void onError(Session session, Throwable t, @PathParam("Uuid") String clientId, @PathParam("speedFlag") boolean speedFlag) {
		System.out.println("mediator: closed websocket channel for client " + t.getMessage() + t.getCause() +clientId);
		peers.remove(new Peer(session, clientId, speedFlag));
	}




	
	
	
}
