-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Lug 14, 2015 alle 11:38
-- Versione del server: 5.6.21
-- PHP Version: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `speedtest`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `measure`
--

CREATE TABLE IF NOT EXISTS `measure` (
`id` bigint(20) unsigned NOT NULL,
  `clientAddress` varchar(20) NOT NULL DEFAULT '',
  `asnum` varchar(50) NOT NULL DEFAULT '',
  `serverAddress` varchar(20) NOT NULL DEFAULT '',
  `serverDomain` varchar(50) NOT NULL DEFAULT '',
  `url` varchar(100) NOT NULL DEFAULT '',
  `size` bigint(20) NOT NULL,
  `duration` bigint(20) NOT NULL,
  `downloadSpeed` int(11) DEFAULT NULL,
  `connectTime` bigint(20) NOT NULL,
  `resourceType` varchar(40) NOT NULL DEFAULT '',
  `uuid` bigint(20) unsigned NOT NULL,
  `timestamp` bigint(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `measure`
--

INSERT INTO `measure` (`id`, `clientAddress`, `asnum`, `serverAddress`, `serverDomain`, `url`, `size`, `duration`, `downloadSpeed`, `connectTime`, `resourceType`, `uuid`, `timestamp`) VALUES
(1, '10.1.1.1', '3', '10.1.1.5', 'polito', 'www.unamilionata.it', 100, 200, 1, 5, 'img', 1, 123123),
(3, '10.1.1.1', '3', '10.1.1.5', 'polito', 'dasdasd', 4, 4, 4, 4, 'img', 1, 123123);

-- --------------------------------------------------------

--
-- Struttura della tabella `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` bigint(20) unsigned NOT NULL,
  `username` varchar(50) NOT NULL DEFAULT '',
  `password` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(20) NOT NULL DEFAULT '',
  `surname` varchar(20) NOT NULL DEFAULT '',
  `enabled` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `name`, `surname`, `enabled`) VALUES
(1, 'admin', 'admin', 'admin', 'admin', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `user_roles`
--

CREATE TABLE IF NOT EXISTS `user_roles` (
`id` bigint(20) unsigned NOT NULL,
  `user` bigint(20) unsigned NOT NULL,
  `role` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `user_roles`
--

INSERT INTO `user_roles` (`id`, `user`, `role`) VALUES
(4, 1, 'ROLE_ADMIN');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `measure`
--
ALTER TABLE `measure`
 ADD PRIMARY KEY (`id`), ADD KEY `users_measures` (`uuid`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
 ADD PRIMARY KEY (`id`), ADD KEY `user_roles` (`user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `measure`
--
ALTER TABLE `measure`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user_roles`
--
ALTER TABLE `user_roles`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `measure`
--
ALTER TABLE `measure`
ADD CONSTRAINT `users_measures` FOREIGN KEY (`uuid`) REFERENCES `users` (`id`);

--
-- Limiti per la tabella `user_roles`
--
ALTER TABLE `user_roles`
ADD CONSTRAINT `user_roles` FOREIGN KEY (`user`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
