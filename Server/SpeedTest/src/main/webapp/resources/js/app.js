google.load('visualization', '1', {packages:['corechart']});

google.setOnLoadCallback(function() {
    angular.bootstrap(document.body, ['speedTestApp']);
});

var speedTestApp = angular.module('speedTestApp', ['ui.bootstrap','ngRoute', 'ngResource','downloadSpeedAverage','downloadSpeedFrequency','downloadLatency','domainAverage','domainSizeAverage','measureTable','login','registration']);

speedTestApp.config(['$routeProvider', '$httpProvider', function($routeProvider, $httpProvider){

    $routeProvider.when('/measures', {
        templateUrl: 'partials/measures.html',
        controller: 'MeasuresController'
    }).when('/averageChart/:provider/:visualization/:offset', {
        templateUrl: 'partials/averageChart.html',
        controller: 'AverageChartController'
    }).when('/home', {
        templateUrl: 'partials/Homepage.html',
        //controller: 'HomePageController'
    }).when('/home/:registration', {
        templateUrl: 'partials/Homepage.html',
        //controller: 'HomePageController'
    }).when('/averageChart', {
        templateUrl: 'partials/averageChart.html',
        controller: 'AverageChartController'
    }).when('/domainChart', {
        templateUrl: 'partials/domainChart.html',
        controller: 'DomainChartController'
    }).when('/domainSizeChart', {
        templateUrl: 'partials/domainSizeChart.html',
        controller: 'DomainSizeChartController'
    }).when('/frequencyChart/:provider/:visualization',{
    	templateUrl: 'partials/frequencyChart.html',
        controller: 'FrequencyChartController'
    }).when('/frequencyChart',{
    	templateUrl: 'partials/frequencyChart.html',
        controller: 'FrequencyChartController'
    }).when('/latencyChart/:provider/:visualization',{
    	templateUrl: 'partials/latencyChart.html',
        controller: 'LatencyChartController'
    }).when('/latencyChart',{
    	templateUrl: 'partials/latencyChart.html',
        controller: 'LatencyChartController'
    }).when('/registration', {
        templateUrl: 'partials/registration.html',
        controller: 'registrationController'
    });

    $routeProvider.otherwise({redirectTo: '/home'});
    
    $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
}])
.controller('CarouselDemoCtrl', ['$scope', '$routeParams', function ($scope, $routeParams) {
	  $scope.myInterval = 4000;
	  $scope.slides = [];
	  $scope.slides.push({image: 'images/AccessDomain.PNG'});
	  $scope.slides.push({image: 'images/DownloadLatency.PNG'});
	  $scope.slides.push({image: 'images/Table3.PNG'});
	  
	  if($routeParams.registration){
		  alertify.alert('Account succesfully activated!');
	  }
}])
.factory('providersFactory',['$resource', function ($resource) {
	 var resource =  $resource('/SpeedTest/measures/providers/:id', {});
	 var factory = {};
	    
	 factory.getResource = function (scope)  {
		 return resource;
	 };
	 return factory; 	// return the factory !
}]);


//speedTestApp

function getDayFromTimestamp(timestamp){
	var day = moment(timestamp);
    day.hours(0);
    day.minutes(0);
    day.seconds(0);
    day.milliseconds(0);
    return day;
}

function getDayFromMoment(m){
	m.hours(0);
    m.minutes(0);
    m.seconds(0);
    m.milliseconds(0);
    return m;
}

function getEndDayFromMoment(m){
    m.hours(23);
    m.minutes(59);
    m.seconds(59);
    m.milliseconds(999);
    return m;
}

function calculateDownloadSpeedAverage(measuresArray){
	var sum = 0;
	for(var i = 0; i < measuresArray.length; i ++) {
        sum += measuresArray[i].downloadSpeed;
    }
	return sum/measuresArray.length;
}
