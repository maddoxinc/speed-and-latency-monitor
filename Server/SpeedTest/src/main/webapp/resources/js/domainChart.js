angular.module('domainAverage',[])

.controller('DomainChartController',['$scope','$rootScope','$routeParams','$location','$timeout','domainFactory',function ($scope, $rootscope, $routeParams,$location, $timeout,domainFactory) {
	var end;
	var start;
	var resource = domainFactory.getResource();
	var measures = {};
    
	//Definition of chart parameters and data
	var data =  google.visualization.arrayToDataTable([
		[{'label':'Domain','type':'string'}, {'label':'Frequency','type':'number'}]
	]);

	var options = {
		title: 'Access domain frequency',
		height:'400',
		chartArea: {left:60,top:60},
		is3D:true,
		sliceVisibilityThreshold: .01,
		reverseCategories: true
	};

    var chart = {};
    chart.data = data;
    chart.options = options;
    chart.realTime = true;
    $scope.chart = chart;
	
	
	if($rootscope.authenticated) {
		resource.query({id:$rootscope.userId},function(measuresList){
			for(var i = 0; i < measuresList.length; i++){
				var measure = measuresList[i];
				measures[measure.serverDomain] = measure.frequency;
				$scope.chart.data.addRows([[measure.serverDomain, measure.frequency]])
			}
		});

		//Web socket for real time charts
	    var ws = new WebSocket("wss://"+ $location.host() + ":" + $location.port() + "/SpeedTest/realTimeMeasures/" + $rootscope.userId + "/" + false);
	 
	    ws.onopen = function () {
	        console.log('websoket aperto');
	    };

	    ws.onclose = function () {
	        console.log('websocket chiuso');
	    };

	    ws.onmessage =  function(event){
	    	console.log('Ricevuto messaggio da websocket: ' + event.data);
	    	if($scope.chart.realTime){
	    		var measureList = JSON.parse(event.data);
		        for(i=0; i<measureList.length; i++){
		        	var measure = measureList[i];
			        if(!measures[measure.serverDomain]){
			        	measures[measure.serverDomain] = 1;
			        	$scope.chart.data.addRows([[measure.serverDomain, 1]])
			       	}else{
			        	measures[measure.serverDomain] += 1;
			        	var foundRows =  $scope.chart.data.getFilteredRows([{column: 0, value: measure.serverDomain}]);
				       	$scope.chart.data.setValue(foundRows[0], 1, measures[measure.serverDomain]);
			       	}
			       	$scope.$apply();
		        }
	    	}
	    };
	}
   

	$scope.setRealTime = function(){}
	
    //Event for responsive chart
    $(window).resize(function(){
    	 var view = new google.visualization.DataView($scope.chart.data);

     	 var chart = new google.visualization.PieChart(document.getElementById("domainChart"));
         chart.draw(view, $scope.chart.options);
    });
}])

.directive('domainChart', function ($location,$timeout) {
    return {
        restrict: 'AE',
        link: function ($scope, elm, attrs) {
            $scope.$watch('chart', function() {
            	console.log("chart changed " + $scope.chart.data);
            	
                var view = new google.visualization.DataView($scope.chart.data);
            	var chart = new google.visualization.PieChart(document.getElementById("domainChart"));
                chart.draw(view, $scope.chart.options);
            },true);
        }
    }
})

.factory('domainFactory',['$resource', function ($resource) {

    var resource =  $resource('/SpeedTest/measures/domain/:id', {});

    var factory = {};
    
    factory.getResource = function (scope)  {
    	return resource;
    };
        

    return factory; 	// return the factory !
}])
