angular.module('domainSizeAverage',[])
.controller('DomainSizeChartController',['$scope','$rootScope','$routeParams','$location','$timeout','domainSizeFactory',function ($scope, $rootscope, $routeParams,$location, $timeout,domainSizeFactory) {
	var resource = domainSizeFactory.getResource();
	var measures = {};
	//Definition of chart parameters and data
	var data =  google.visualization.arrayToDataTable([
		[{'label':'Domain','type':'string'}, {'label':'Frequency','type':'number'}]
	]);

	var options = {
		title: 'Domain downloaded data',
		height:'400',
		chartArea: {left:60,top:60},
		is3D:true,
		sliceVisibilityThreshold: .01,
		reverseCategories: true
	};

    var chart = {};
    chart.data = data;
    chart.options = options;
    chart.realTime = true;
    $scope.chart = chart;
    
    if($rootscope.authenticated) {
    	resource.query({id:$rootscope.userId},function(measuresList){
    		for(var i = 0; i < measuresList.length; i++){
    			var measure = measuresList[i];
    			measures[measure.serverDomain] = measure.totalDownloadSize;
    			$scope.chart.data.addRows([[measure.serverDomain, measure.totalDownloadSize]]);
    			
    			if(measure.totalDownloadSize < 1048576)
    			{
	    			$scope.chart.data.setFormattedValue(i,1,Math.floor(measure.totalDownloadSize/1024)  + " kB");
    			}
    			else
    			{
	    			$scope.chart.data.setFormattedValue(i,1,Number(Math.round(((measure.totalDownloadSize/1048576))+'e2')+'e-2') + " MB");
    			}
    		}
    	});
    	
		//Web socket for real time charts
	    var ws = new WebSocket("wss://"+ $location.host() + ":" + $location.port() + "/SpeedTest/realTimeMeasures/" + $rootscope.userId + "/" + false);
	 
	    ws.onopen = function () {
	        console.log('websoket aperto');
	    };

	    ws.onclose = function () {
	        console.log('websocket chiuso');
	    };

	    ws.onmessage =  function(event){
	    	console.log('Ricevuto messaggio da websocket: ' + event.data);
	    	if($scope.chart.realTime){
		        var measureList = JSON.parse(event.data);
		        for(i=0; i<measureList.length; i++){
		        	var measure = measureList[i];
		        	if(measure.size > 0){
			        	if(!measures[measure.serverDomain]){
			        		measures[measure.serverDomain] = measure.size;
			        		$scope.chart.data.addRows([[measure.serverDomain, measure.size]]);
			        		var foundRows =  $scope.chart.data.getFilteredRows([{column: 0, value: measure.serverDomain}]);
			    			
			        		if(measure.size < 1048576)
			    			{
			        			$scope.chart.data.setFormattedValue(foundRows[0],1,Math.floor(measures[measure.serverDomain]/1024)  + " kB");
			    			}
			    			else
			    			{
			    				$scope.chart.data.setFormattedValue(foundRows[0],1,Number(Math.round(((measure.totalDownloadSize/1048576))+'e2')+'e-2') + " MB");
			    			}
				        	

			        	}else{
			        		measures[measure.serverDomain] += measure.size;
			        		var foundRows =  $scope.chart.data.getFilteredRows([{column: 0, value: measure.serverDomain}]);
				        	$scope.chart.data.setValue(foundRows[0], 1, measures[measure.serverDomain]);
				        	
			        		if(measure.size < 1048576)
			    			{
			        			$scope.chart.data.setFormattedValue(foundRows[0],1,Math.floor(measures[measure.serverDomain]/1024)  + " kB");
			    			}
			    			else
			    			{
			    				$scope.chart.data.setFormattedValue(foundRows[0],1,Number(Math.round(((measure.totalDownloadSize/1048576))+'e2')+'e-2') + " MB");
			    			}
			        	}
			        	$scope.$apply();
		        	}
		        }
	    	}
	    };
	}
   
    $scope.setRealTime = function(){}
    
    //Event for responsive chart
    $(window).resize(function(){
    	 var view = new google.visualization.DataView($scope.chart.data);

     	 var chart = new google.visualization.PieChart(document.getElementById("domainSizeChart"));
         chart.draw(view, $scope.chart.options);
    });
}])
.directive('domainsizeChart', function ($location,$timeout) {
    return {
        restrict: 'AE',
        link: function ($scope, elm, attrs) {
            $scope.$watch('chart', function() {
            	console.log("chart changed " + $scope.chart.data);
            	
                var view = new google.visualization.DataView($scope.chart.data);
            	var chart = new google.visualization.PieChart(document.getElementById("domainSizeChart"));
                chart.draw(view, $scope.chart.options);
            },true);
        }
    }
})

.factory('domainSizeFactory',['$resource', function ($resource) {

    var resource =  $resource('/SpeedTest/measures/domainSize/:id', {});

    var factory = {};
    
    factory.getResource = function (scope)  {
    	return resource;
    };
        

    return factory; 	// return the factory !
}])