angular.module('downloadLatency',[])
.controller('LatencyChartController',['$scope','$rootScope','$routeParams','$location','$timeout','providersFactory','latencyFactory',function ($scope, $rootscope, $routeParams,$location, $timeout,providersFactory,latencyFactory) {
	var providersResource = providersFactory.getResource();
	var latencies = {};
	var frequencyResource = latencyFactory.getResource();
	var interval,start, end;
	$scope.userCount = 0;
	$scope.globalCount = 0;
	
	//Definition of chart parameters and data
	//var data =  google.visualization.arrayToDataTable([[{'label':'Day','type':'string'}, {'label':'Download speed average','type':'number'}, {'label':'Global average','type':'number'},{'type': 'string', 'role': 'style'},{type: 'string', role: 'annotation'}]]);
	var data =  google.visualization.arrayToDataTable([[{'label':'Latency','type':'string'}, {'label':'User frequencies','type':'number'}, {'type':'boolean', 'role':'scope'},{'type': 'string', 'role': 'tooltip'}]]); 
	var options = {
        height:'400',
        chartArea: {left:60,top:60},
        seriesType: 'bars',
        vAxis: {
            minValue:0,
            viewWindow: {
                min: 0
            },
            format: 'percent',
            title: 'Frequency'
        },
        hAxis: {title: 'Download Latency [ms]'}
    };
    var chart = {};
    chart.data = data;
    chart.options = options;
    chart.realTime = true;
    $scope.chart = chart;
    
    //Definition of chart visualization types
    $scope.chartTypes = [
    	{typeName: '0-1000 ms', typeValue: '0-1000 ms'},
    	{typeName: '1000-2000 ms', typeValue: '1000-2000 ms'},
    	{typeName: '2000-3000 ms', typeValue: '2000-3000 ms'},
    	{typeName: '3000-4000 ms', typeValue: '3000-4000 ms'},
    	{typeName: '4000-5000 ms', typeValue: '4000-5000 ms'}

    ];
    
    //Set the start visualization type
	if($routeParams.visualization != undefined){
		var index = 0;
		for(var i = 0; i < $scope.chartTypes.length; i++){
			if($scope.chartTypes[i].typeName == $routeParams.visualization){
				index = i;
				break;
			}	
		}
		$scope.visualization = $scope.chartTypes[index];
	}
	else{
		$scope.visualization = $scope.chartTypes[0];
	}
	
	if($scope.visualization.typeName == "0-1000 ms"){
		interval = 0;	
		start = 0;
		end = 1000;
	}else if($scope.visualization.typeName == "1000-2000 ms"){
		interval = 1;
		start = 1000;
		end = 2000;
	}else if($scope.visualization.typeName == "2000-3000 ms"){
		interval = 2;
		start = 2000;
		end = 3000;
	}else if($scope.visualization.typeName == "3000-4000 ms"){
		interval = 3;
		start = 3000;
		end = 4000;
	}else if($scope.visualization.typeName == "4000-5000 ms"){
		interval = 4;
		start = 4000;
		end = 5000;
	}else{
		interval = 0;
		start = 0;
		end = 5000;
	}
	
	for(var index = start; index <= end; index += 50){
    	$scope.chart.data.addRows([[index + "-" + Number(Number(index)+49),0,false,'No measures in this latency interval']]);
	}
	
    //Get providers
	var providerPromise;
	if(!$rootscope.providers) {
		//Definition of providers array
	    $rootscope.providers = [];
		providerPromise = providersResource.query({id:$rootscope.userId}, function (providersList) {
			if(providersList.length>0){
				for (var i = 0; i < providersList.length; i++) {
					$rootscope.providers.push({typeName: providersList[i], typeValue: providersList[i]});
				}
				if (!$routeParams.provider || $routeParams.provider == "unknown")
					$scope.currentProvider = $rootscope.providers[0];
				else {
					var index = 0;
					for (var i = 0; i < $rootscope.providers.length; i++) {
						if ($rootscope.providers[i].typeName == $routeParams.provider) {
							index = i;
							break;
						}
					}
					$scope.currentProvider = $rootscope.providers[index];
				}
			}
		});
	}else{
		//define the selected provider when the providers are already loaded
		if($rootscope.providers.length>0){
			if ($routeParams.provider == undefined)
				$scope.currentProvider = $rootscope.providers[0];
			else {
				var index = 0;
				for (var i = 0; i < $rootscope.providers.length; i++) {
					if ($rootscope.providers[i].typeName == $routeParams.provider) {
						index = i;
						break;
					}
				}
				$scope.currentProvider = $rootscope.providers[index];
			}
		}
	}
	
	if($rootscope.authenticated){
		if(providerPromise){
			providerPromise.$promise.then(getLatencies);
		}else{
			getLatencies();
		}
	}
	
	$scope.setRealTime = function(){};
	
	$scope.selectType = function(type) {
		$scope.offset = 0;

		if(providerPromise){
			providerPromise.$promise.then(function(data){
		        $scope.visualization = type;
		        if($scope.currentProvider)
		        	$location.path("/latencyChart/" + $scope.currentProvider.typeName + "/" + $scope.visualization.typeName);
		        else
		        	$location.path("/latencyChart/unknown/" + $scope.visualization.typeName);
			});
		}
		else{
			$scope.visualization = type;
			if($scope.currentProvider)
		    	$location.path("/latencyChart/" + $scope.currentProvider.typeName + "/" + $scope.visualization.typeName);
		    else
		    	$location.path("/latencyChart/unknown/" + $scope.visualization.typeName);		
		}
    };
    
	$scope.selectProvider = function(provider){
		if(providerPromise){
			providerPromise.$promise.then(function(data){
				$scope.currentProvider = provider;
		    	$location.path("/latencyChart/" + $scope.currentProvider.typeName+ "/" + $scope.visualization.typeName);
			});
		}
		else{
			$scope.currentProvider = provider;
	    	$location.path("/latencyChart/" + $scope.currentProvider.typeName+ "/" + $scope.visualization.typeName);
		}
	};
	
	//Web socket for real time charts
	if($rootscope.authenticated){
	    var ws = new WebSocket("wss://"+ $location.host() + ":" + $location.port() + "/SpeedTest/realTimeMeasures/" + $rootscope.userId + "/" + false );
	 
	    ws.onopen = function () {
	        console.log('websoket aperto');
	    };
	
	    ws.onclose = function () {
	        console.log('websocket chiuso');
	    };
	
	    ws.onmessage =  function(event) {
			console.log('Ricevuto messaggio da websocket: ' + event.data);
			if ($scope.chart.realTime) {
				var measureList = JSON.parse(event.data);
				for (i = 0; i < measureList.length; i++) {
					var measure = measureList[i];
					if ($scope.currentProvider && measure.asnum == $scope.currentProvider.typeName) {
						$scope.userCount ++;
						for(var index = start; index <= end; index += 50){
							if(measure.connectTime >= index && measure.connectTime <= index + 49)
								var interval = index + "-" + Number(Number(index)+49);
						}
						
						if(interval){
							if(latencies[interval] == undefined){
								latencies[interval] = {};
								latencies[interval] = 1;
								
							}
							else {
								latencies[interval] ++;	
							}
							
							var keys = Object.keys(latencies);
					    	for(var k = 0; k<keys.length; k++){
					    		var foundRows = $scope.chart.data.getFilteredRows([{
									column: 0,
									value: keys[k]
								}]);
					    		if (foundRows[0]) {
					    			$scope.chart.data.setValue(foundRows[0], 1, latencies[keys[k]]/$scope.userCount);
									$scope.chart.data.setValue(foundRows[0], 2, true);
						        	$scope.chart.data.setValue(foundRows[0], 3, "Latency interval: " + keys[k] + " ms\nUser frequency: "+ Number(Math.round(((latencies[keys[k]]/$scope.userCount)*100) +'e2')+'e-2') + "%");
					    		}
					    	}
							$scope.$apply();
						}	
					}
					else{
						var newProvider = true;
						for(var k = 0; k < $rootscope.providers.length; k++){
							if($rootscope.providers[k].typeName == measure.asnum){
								newProvider = false;
							}	
						}
						if(newProvider){
							$rootscope.providers.push({typeName: measure.asnum, typeValue: measure.asnum});
							
							if(!$scope.currentProvider){
								
								var index = 0;
								for (var k = 0; k < $rootscope.providers.length; k++) {
									if ($rootscope.providers[k].typeName == measure.asnum) {
										index = i;
										break;
									}
								}
								$scope.currentProvider = $rootscope.providers[index];
								
								$scope.userCount ++;
								for(var index = start; index <= end; index += 50){
									if(measure.connectTime >= index && measure.connectTime <= index + 49){
										var interval = index + "-" + Number(Number(index)+49);
										break;
									}
								}
								
								if(interval){
									if(latencies[interval] == undefined){
										latencies[interval] = {};
										latencies[interval] = 1;
										
									}
									else {
										latencies[interval] ++;	
									}
									
									var keys = Object.keys(latencies);
							    	for(var k = 0; k<keys.length; k++){
							    		var foundRows = $scope.chart.data.getFilteredRows([{
											column: 0,
											value: keys[k]
										}]);
							    		if (foundRows[0]) {
							    			$scope.chart.data.setValue(foundRows[0], 1, latencies[keys[k]]/$scope.userCount);
											$scope.chart.data.setValue(foundRows[0], 2, true);
								        	$scope.chart.data.setValue(foundRows[0], 3, "Latency interval: " + keys[k] + " ms\nUser frequency: "+ Number(Math.round(((latencies[keys[k]]/$scope.userCount)*100) +'e2')+'e-2') + "%");
							    		}
							    	}
							    	
							    	
								}

							}
							$scope.$apply();

						}
						
						
						
					}
				}
			}
		};
	}
	
	//Event for responsive chart
    $(window).resize(function(){
    	 var view = new google.visualization.DataView($scope.chart.data);
     	 var chart = new google.visualization.ColumnChart(document.getElementById("latencyChart"));
         chart.draw(view, $scope.chart.options);
    });
    
    //Get latencies from the server
	function getLatencies(){
		if($scope.currentProvider){
			
		    frequencyResource.query({asnum:$scope.currentProvider.typeName,id:$rootscope.userId,interval:interval},function(latenciesList){
		    	for(var i = 0; i < latenciesList.length; i++){
		    		var freq = latenciesList[i];
		    		$scope.userCount = freq.total;
		    		latencies[freq.latencyInterval] = freq.frequency;
		    		var latency = freq.latencyInterval;
		    		var count = freq.frequency/$scope.userCount;
		    		var foundRows =  $scope.chart.data.getFilteredRows([{column: 0, value: latency}]);
			        if(foundRows[0] != undefined){
			        	$scope.chart.data.setValue(foundRows[0], 1, Number(count));
			        	$scope.chart.data.setValue(foundRows[0], 2, true);
			        	$scope.chart.data.setValue(foundRows[0], 3, "Latency Interval: " + latency + " ms\nUser frequency: "+ Number(Math.round((count*100)+'e2')+'e-2') + "%");
			        }
		    	}
		    })
		    
}
	}
	
    
}]).factory('latencyFactory',['$resource', function ($resource) {
	var resource =  $resource('/SpeedTest/measures/userLatencies/:asnum/:id/:interval', {});
	 var factory = {};
	    
	 factory.getResource = function (scope)  {
		 return resource;
	 };
	 return factory; 	// return the factory !
}])   
.directive('dslatencyChart', function ($location) {
    return {
        restrict: 'AE',
        link: function ($scope, elm, attrs) {
            $scope.$watch('chart', function() {
            	console.log("chart changed " + $scope.chart.data);
            	
                var view = new google.visualization.DataView($scope.chart.data);
            	var chart = new google.visualization.ColumnChart(document.getElementById("latencyChart"));
                chart.draw(view, $scope.chart.options);
            },true);
        }
    };
})
