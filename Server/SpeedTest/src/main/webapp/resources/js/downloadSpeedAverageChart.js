angular.module('downloadSpeedAverage',[])

.controller('AverageChartController',['$scope','$rootScope','$routeParams','$location','$timeout','staticMeasuresFactory','globalAveragesFactory','providersFactory',function ($scope, $rootscope, $routeParams,$location, $timeout,staticMeasuresFactory,globalAveragesFactory,providersFactory) {
	var end;
	var start;
	var resource = staticMeasuresFactory.getResource();
	var globlaAveragesResource = globalAveragesFactory.getResource();
	var providersResource = providersFactory.getResource();
	var measures = {};
    
	//Definition of chart parameters and data
	var data =  google.visualization.arrayToDataTable([[{'label':'Day','type':'string'}, {'label':'Download speed average','type':'number'}, {'type':'boolean', 'role':'scope'},{'type': 'string', 'role': 'style'},{'type': 'string', 'role': 'tooltip'}, {'label':'Global average','type':'number'}, {'type':'boolean', 'role':'scope'} ,{'type': 'string', 'role': 'style'},{'type': 'string', 'role': 'tooltip'}]]); 
	var options = {
        height:'400',
        chartArea: {left:60,top:60},
        pointSize: 8,
        seriesType: 'line',

        series: {
            0: { color: 'blue' },
            1: { color: 'green' }
        },
        /*seriesType: 'bars',
        series: {1: {type: 'line'}},*/
        vAxis: {
            minValue:0,
            viewWindow: {
                min: 0
            }
        }
    };
    var chart = {};
    chart.data = data;
    chart.options = options;
    if(!$rootscope.global || $rootscope.global == false)
    	chart.globalAverage = false;
    else
    	chart.globalAverage = true;
    chart.realTime = true;
    $scope.chart = chart;
    
    
    
    //Definition of chart visualization types
    $scope.chartTypes = [
    	{typeName: 'Weekly', typeValue: 'Weekly'},
    	{typeName: 'Monthly', typeValue: 'Monthly'},
    	{typeName: 'Multi-month', typeValue: 'Multi-month'},
    ];
    $scope.visualization = {};

    //Set the start visualization type
	if($routeParams.visualization){
		var index = 0;
		for(var i = 0; i < $scope.chartTypes.length; i++){
			if($scope.chartTypes[i].typeName == $routeParams.visualization){
				index = i;
				break;
			}	
		}
		$scope.visualization = $scope.chartTypes[index];
	}
	else{
		$scope.visualization = $scope.chartTypes[0];
	}
		
	//Set the current offset
	if(isNaN($routeParams.offset)){
		$scope.offset = 0;
	}
	else{
		$scope.offset = $routeParams.offset;
	}
	
	
	//Set of the start and the end date for the current visualization
	if($scope.visualization.typeName == "Weekly"){
		end = getEndDayFromMoment(moment.utc());
		end.add($scope.offset,'w').calendar();
		start = getDayFromMoment(end.clone());
		start.add(-6,'d');
		
	}else if($scope.visualization.typeName == "Monthly"){
		end = getEndDayFromMoment(moment());
		end.add($scope.offset,'M').calendar();
		start = getDayFromMoment(end.clone());
		start.add(-1,'M');
		start.add(1,'d');
	}else if($scope.visualization.typeName == "Multi-month"){
		end = getEndDayFromMoment(moment());
		end.add($scope.offset*3,'M').calendar();
		start = getDayFromMoment(end.clone());
		start.add(-3,'M');
		start.add(1,'d');
	}
	
	//Add initial rows from start to end date in the chart datatable 
	var index = start.clone();
	while(!index.isAfter(end)){
		console.log("while");
		$scope.chart.data.addRows([[index.format("D/M/YYYY"),0,false,'point {visible: false }','No Measures in this day',0,false, 'point {visible: false }','No Measures in this day']]);
		index.add(1,'d');
	}
    
	//Get providers
	var providerPromise;
	if(!$rootscope.providers) {
		//Definition of providers array
	    $rootscope.providers = [];
		providerPromise = providersResource.query({id:$rootscope.userId}, function (providersList) {
			if(providersList.length>0){
				for (var i = 0; i < providersList.length; i++) {
					$rootscope.providers.push({typeName: providersList[i], typeValue: providersList[i]});
				}
				if (!$routeParams.provider || $routeParams.provider == "unknown")
					$scope.currentProvider = $rootscope.providers[0];
				else {
					var index = 0;
					for (var i = 0; i < $rootscope.providers.length; i++) {
						if ($rootscope.providers[i].typeName == $routeParams.provider) {
							index = i;
							break;
						}
					}
					$scope.currentProvider = $rootscope.providers[index];
				}
			}
		});
	}else{
		//define the selected provider when the providers are already loaded
		if($rootscope.providers.length>0){
			if (!$routeParams.provider)
				$scope.currentProvider = $rootscope.providers[0];
			else {
				var index = 0;
				for (var i = 0; i < $rootscope.providers.length; i++) {
					if ($rootscope.providers[i].typeName == $routeParams.provider) {
						index = i;
						break;
					}
				}
				$scope.currentProvider = $rootscope.providers[index];
			}
		}
	}
	
	if($rootscope.authenticated){
		if(providerPromise){
			providerPromise.$promise.then(getMeasures);
			providerPromise.$promise.then(getGlobalMeasures);

		}else{
			getMeasures();
			getGlobalMeasures();
		}
	}
	
	$scope.setGlobalAverages = function(){
		
		if($scope.chart.globalAverage){
			if(providerPromise){
				providerPromise.$promise.then(getGlobalMeasures);
			}else{
				getGlobalMeasures();
			}
		}
		
		if(!$rootscope.global || $rootscope.global == false)
			$rootscope.global = true;
		else
			$rootscope.global = false;
	}
	
	//Get the average from the server
	function getMeasures(){
		if($scope.currentProvider){
			resource.query({asnum:$scope.currentProvider.typeName,id:$rootscope.userId,start:start.unix()*1000,end:end.unix()*1000},function(measuresList){
			    for(var i = 0; i < measuresList.length; i++){
			    	var measure = measuresList[i];
			    	
			        var day = getDayFromTimestamp(measure.timestamp);
			        var downloadSpeedAverage = measure.sum/measure.totalMeasures;
			        measures[day] = measure;
			        var foundRows =  $scope.chart.data.getFilteredRows([{column: 0, value: day.format("D/M/YYYY")}]);
			        if(foundRows[0]){
			        	$scope.chart.data.setValue(foundRows[0], 1, downloadSpeedAverage);
			        	$scope.chart.data.setValue(foundRows[0], 2, true);
			        	$scope.chart.data.setValue(foundRows[0], 3, 'point {visible: true }');
			        	$scope.chart.data.setValue(foundRows[0], 4, day.format('DD/MM/YYYY')+"\n"+"User download speed average: "+ Number(Math.round((downloadSpeedAverage)+'e2')+'e-2') +" Mbit/s" );
			        }         
			    }
			});
		}
	}
	
	//Function to get global averages from the server
	function getGlobalMeasures(){
		if($scope.chart.globalAverage && $scope.currentProvider){
			globlaAveragesResource.query({asnum:$scope.currentProvider.typeName,start:start.unix()*1000,end:end.unix()*1000},function(measuresList){
				for(var i = 0; i < measuresList.length; i++){
					var measure = measuresList[i];
			        var day = getDayFromTimestamp(measure.timestamp);
			        var downloadSpeedAverage = measure.sum/measure.totalMeasures;     
			        var foundRows =  $scope.chart.data.getFilteredRows([{column: 0, value: day.format("D/M/YYYY")}]);
			        if(foundRows[0]){
			        	$scope.chart.data.setValue(foundRows[0], 5, downloadSpeedAverage);
			        	$scope.chart.data.setValue(foundRows[0], 6, true);
			        	$scope.chart.data.setValue(foundRows[0], 7, 'point {visible: true }');
			        	$scope.chart.data.setValue(foundRows[0], 8, day.format('DD/MM/YYYY')+"\n"+"Global download speed average: "+Number(Math.round((downloadSpeedAverage)+'e2')+'e-2')+" Mbit/s" );
			        }
			}
			});
		}
	}
	
	
	$scope.setRealTime = function(){
	};
	
	$scope.selectType = function(type) {
		$scope.offset = 0;

		if(providerPromise){
			providerPromise.$promise.then(function(data){
		        $scope.visualization = type;
		        if($scope.currentProvider)
		        	$location.path("/averageChart/" + $scope.currentProvider.typeName + "/" + $scope.visualization.typeName + "/" + $scope.offset);
		        else
		        	$location.path("/averageChart/unknown/" + $scope.visualization.typeName + "/" + $scope.offset);

		        	
			});
		}
		else{
			$scope.visualization = type;
			if($scope.currentProvider)
				$location.path("/averageChart/" + $scope.currentProvider.typeName + "/" + $scope.visualization.typeName + "/" + $scope.offset);
			else
				$location.path("/averageChart/unknown/" + $scope.visualization.typeName + "/" + $scope.offset);

				
		}
    };
	
	$scope.selectProvider = function(provider){
		wsGlobal.close();
		if(providerPromise){
			providerPromise.$promise.then(function(data){
				$scope.currentProvider = provider;
		    	$location.path("/averageChart/" + $scope.currentProvider.typeName + "/" + $scope.visualization.typeName + "/" + $scope.offset);
			});
		}
		else{
			$scope.currentProvider = provider;
	    	$location.path("/averageChart/" + $scope.currentProvider.typeName + "/" + $scope.visualization.typeName + "/" + $scope.offset);
		}
	};
	
	$scope.next = function(){
		if(providerPromise){
			providerPromise.$promise.then(function(data){
				$scope.offset++;
				if($scope.currentProvider)
					$location.path("/averageChart/" + $scope.currentProvider.typeName + "/" + $scope.visualization.typeName + "/" + $scope.offset);
				else
					$location.path("/averageChart/unknown/" + $scope.visualization.typeName + "/" + $scope.offset);			});
		}
		else{
			$scope.offset++;
			if($scope.currentProvider)
				$location.path("/averageChart/" + $scope.currentProvider.typeName + "/" + $scope.visualization.typeName + "/" + $scope.offset);
			else
				$location.path("/averageChart/unknown/" + $scope.visualization.typeName + "/" + $scope.offset);
		}
	};
	
	$scope.prev = function(){
		if(providerPromise){
			providerPromise.$promise.then(function(data){
				$scope.offset--;
				if($scope.currentProvider)
					$location.path("/averageChart/" + $scope.currentProvider.typeName + "/" + $scope.visualization.typeName + "/" + $scope.offset);
				else
					$location.path("/averageChart/unknown/" + $scope.visualization.typeName + "/" + $scope.offset);			});
		}
		else{
			$scope.offset--;
			if($scope.currentProvider)
				$location.path("/averageChart/" + $scope.currentProvider.typeName + "/" + $scope.visualization.typeName + "/" + $scope.offset);
			else
				$location.path("/averageChart/unknown/" + $scope.visualization.typeName + "/" + $scope.offset);		}
	};
	
	
	//Web socket for real time charts
	if($rootscope.authenticated){
	    var ws = new WebSocket("wss://"+ $location.host() + ":" + $location.port() + "/SpeedTest/realTimeMeasures/" + $rootscope.userId + "/" + true );
	 
	    ws.onopen = function () {
	        console.log('websoket aperto');
	    };
	
	    ws.onclose = function () {
	        console.log('websocket chiuso');
	    };
	
	    ws.onmessage =  function(event) {
			console.log('Ricevuto messaggio da websocket: ' + event.data);
			if ($scope.chart.realTime) {
				var measureList = JSON.parse(event.data);
				for (i = 0; i < measureList.length; i++) {
					var measure = measureList[i];
					if ( $scope.currentProvider && measure.asnum == $scope.currentProvider.typeName ) {
						var day = getDayFromTimestamp(measure.timestamp);

						if (!measures[day]) {
							measures[day] = {};
							measures[day].sum = measure.downloadSpeed;
							measures[day].totalMeasures = 1;
							var foundRows = $scope.chart.data.getFilteredRows([{
								column: 0,
								value: day.format("D/M/YYYY")
							}]);
							if (foundRows[0]) {
								$scope.chart.data.setValue(foundRows[0], 1, measures[day].sum);
								$scope.chart.data.setValue(foundRows[0], 2, true);
								$scope.chart.data.setValue(foundRows[0], 3, 'point {visible: true }');
								$scope.chart.data.setValue(foundRows[0], 4, day.format('DD/MM/YYYY') + "\n" + "User download speed average: " + Number(Math.round((measures[day].sum)+'e2')+'e-2') + " Mbit/s");
								$scope.$apply();
							}
						}
						else {
							measures[day].sum += measure.downloadSpeed;
							measures[day].totalMeasures++;
							var downloadSpeedAverage = measures[day].sum / measures[day].totalMeasures;
							var foundRows = $scope.chart.data.getFilteredRows([{
								column: 0,
								value: day.format("D/M/YYYY")
							}]);
							$scope.chart.data.setValue(foundRows[0], 1, downloadSpeedAverage);
							$scope.chart.data.setValue(foundRows[0], 4, day.format('DD/MM/YYYY') + "\n" + "User download speed average: " + Number(Math.round((downloadSpeedAverage)+'e2')+'e-2') + " Mbit/s");
							$scope.$apply();
						}
					}
					else{
						var newProvider = true;
						for(var i = 0; i < $rootscope.providers.length; i++){
							if($rootscope.providers[i].typeName == measure.asnum){
								newProvider = false;
							}	
						}
						if(newProvider){
							$rootscope.providers.push({typeName: measure.asnum, typeValue: measure.asnum});
							if(!$scope.currentProvider){
								
								var index = 0;
								for (var i = 0; i < $rootscope.providers.length; i++) {
									if ($rootscope.providers[i].typeName == measure.asnum) {
										index = i;
										break;
									}
								}
								$scope.currentProvider = $rootscope.providers[index];
								
								connectGlobalSocket();
								getGlobalMeasures();

								var day = getDayFromTimestamp(measure.timestamp);

								if (!measures[day]) {
									measures[day] = {};
									measures[day].sum = measure.downloadSpeed;
									measures[day].totalMeasures = 1;
									var foundRows = $scope.chart.data.getFilteredRows([{
										column: 0,
										value: day.format("D/M/YYYY")
									}]);
									if (foundRows[0]) {

										$scope.chart.data.setValue(foundRows[0], 1, measures[day].sum);
										$scope.chart.data.setValue(foundRows[0], 2, true);
										$scope.chart.data.setValue(foundRows[0], 3, 'point {visible: true }');
										$scope.chart.data.setValue(foundRows[0], 4, day.format('DD/MM/YYYY') + "\n" + "User download speed average: " + Number(Math.round((measures[day].sum)+'e2')+'e-2') + " Mbit/s");
									}

								}
							}
							$scope.$apply();


						}
					}
					
				}
			}
		};
	}
	
	//Web socket for global real time charts
	if(providerPromise){
		providerPromise.$promise.then(connectGlobalSocket);
	}else{
		connectGlobalSocket();
	}
	
	
	var wsGlobal;
	function connectGlobalSocket(){
		if($scope.currentProvider){
		    wsGlobal = new WebSocket("wss://"+ $location.host() + ":" + $location.port() + "/SpeedTest/globalRealTimeMeasures/" + $scope.currentProvider.typeName +"/average");
		 
		    wsGlobal.onopen = function () {
		        console.log('websoket global aperto');
		    };
		
		    wsGlobal.onclose = function () {
		        console.log('websocket global chiuso');
		    };
		
		    wsGlobal.onmessage =  function(event){
		    	console.log('Ricevuto messaggio da websocket global: ' + event.data);
		    	
		    	if($scope.chart.realTime){
		    		var averageList = [];
		    		averageList = JSON.parse(event.data);
		    		for(var i = 0; i < averageList.length; i++){
		    			var measure = averageList[i];
		    			var day = getDayFromTimestamp(measure.timestamp);
						var foundRows =  $scope.chart.data.getFilteredRows([{column: 0, value: day.format("D/M/YYYY")}]);
						if(foundRows[0]){
							$scope.chart.data.setValue(foundRows[0], 5, measure.sum/measure.totalMeasures);
							$scope.chart.data.setValue(foundRows[0], 6, true);
							$scope.chart.data.setValue(foundRows[0], 7, 'point {visible: true }');
							$scope.chart.data.setValue(foundRows[0], 8, day.format('DD/MM/YYYY')+"\n"+"Global download speed average: "+Number(Math.round((measure.sum/measure.totalMeasures)+'e2')+'e-2') +" Mbit/s" );
						}
		    		}
					$scope.$apply();

			        
					
		    	}
		    };
		}
	}
    
    //Event for responsive chart
    $(window).resize(function(){
    	 var view = new google.visualization.DataView($scope.chart.data);
         if(! $scope.chart.globalAverage) {
			 view.hideColumns([5]);
			 view.hideColumns([6]);
			 view.hideColumns([7]);
			 view.hideColumns([8]);
		 }
         else{
        	 view.setColumns([0,1,2,3,4,5,6,7,8]);
         }
     	 var chart = new google.visualization.ComboChart(document.getElementById("averageChart"));
         chart.draw(view, $scope.chart.options);
    });
    
    
}])

.directive('dsaverageChart', function ($location,$timeout) {
    return {
        restrict: 'AE',
        link: function ($scope, elm, attrs) {
            $scope.$watch('chart', function() {
            	console.log("chart changed " + $scope.chart.data);
            	
                var view = new google.visualization.DataView($scope.chart.data);
                if(! $scope.chart.globalAverage) {
					view.hideColumns([5]);
					view.hideColumns([6]);
					view.hideColumns([7]);
					view.hideColumns([8]);
				}
                else{
                	view.setColumns([0,1,2,3,4,5,6,7,8]);
                }
            	var chart = new google.visualization.ComboChart(document.getElementById("averageChart"));
            	
                chart.draw(view, $scope.chart.options);
            },true);
        }
    };
})

.factory('staticMeasuresFactory',['$resource', function ($resource) {

    var resource =  $resource('/SpeedTest/measures/userAverages/:asnum/:id/:start/:end', {});

    var factory = {};
    
    factory.getResource = function (scope)  {
    	return resource;
    };
    return factory; 	// return the factory !
}])

.factory('globalAveragesFactory',['$resource', function ($resource) {
	 var resource =  $resource('/SpeedTest/measures/globalAverages/:asnum/:start/:end', {});

	 var factory = {};
	    
	 factory.getResource = function (scope)  {
		 return resource;
	 };
	 return factory; 	// return the factory !
}]);


