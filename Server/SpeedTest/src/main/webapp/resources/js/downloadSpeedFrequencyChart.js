angular.module('downloadSpeedFrequency',[])
.controller('FrequencyChartController',['$scope','$rootScope','$routeParams','$location','$http','providersFactory','frequencyFactory','globalFrequencyFactory',function ($scope, $rootscope, $routeParams,$location,$http,providersFactory,frequencyFactory,globalFrequencyFactory) {

	
	var providersResource = providersFactory.getResource();
	var frequencyResource = frequencyFactory.getResource();
	var globalFrequencyResource = globalFrequencyFactory.getResource();
	var start;
	var end;
	var frequencies = {};
	$scope.userCount = 0;
	$scope.globalCount = 0;
	
	//Definition of chart parameters and data
	//var data =  google.visualization.arrayToDataTable([[{'label':'Day','type':'string'}, {'label':'Download speed average','type':'number'}, {'label':'Global average','type':'number'},{'type': 'string', 'role': 'style'},{type: 'string', role: 'annotation'}]]);
	var data =  google.visualization.arrayToDataTable([[{'label':'Speed','type':'number'}, {'label':'User frequencies','type':'number'}, {'type':'boolean', 'role':'scope'},{'type': 'string', 'role': 'tooltip'}, {'label':'Global frequencies','type':'number'}, {'type':'boolean', 'role':'scope'},{'type': 'string', 'role': 'tooltip'}]]); 
	var options = {
        height:'400',
        chartArea: {left:60,top:60},
        seriesType: 'bars',
        //series: {1: {type: 'line'}},
        //curveType: 'function',
        vAxis: {
            minValue:0,
            viewWindow: {
                min: 0
            },
            format: 'percent',
            title: 'Frequency'
        },
        hAxis: {title: 'Download Speed [Mbps]'}
    };
    var chart = {};
    chart.data = data;
    chart.options = options;
    if(!$rootscope.global || $rootscope.global == false)
    	chart.globalFrequency = false;
    else
    	chart.globalFrequency = true;    
    chart.realTime = true;
    $scope.chart = chart;
    
    //Definition of chart visualization types
    $scope.chartTypes = [
    	{typeName: '1-30 Mbps', typeValue: '1-30 Mbps'},
    	{typeName: '30-60 Mbps', typeValue: '30-60 Mbps'},
    	{typeName: '60-90 Mbps', typeValue: '60-90 Mbps'},
    	{typeName: '90-120 Mbps', typeValue: '90-120 Mbps'}

    ];
    
    //Set the start visualization type
	if($routeParams.visualization != undefined){
		var index = 0;
		for(var i = 0; i < $scope.chartTypes.length; i++){
			if($scope.chartTypes[i].typeName == $routeParams.visualization){
				index = i;
				break;
			}	
		}
		$scope.visualization = $scope.chartTypes[index];
	}
	else{
		$scope.visualization = $scope.chartTypes[0];
	}
	
	if($scope.visualization.typeName == "1-30 Mbps"){
		end = 30;
		start = 1;		
	}else if($scope.visualization.typeName == "30-60 Mbps"){
		end = 60;
		start = 30;
	}else if($scope.visualization.typeName == "60-90 Mbps"){
		end = 90;
		start = 60;
	}else if($scope.visualization.typeName == "90-120 Mbps"){
		end = 120;
		start = 90;
	}
	
    for(var index = start; index <= end;index ++){
    	$scope.chart.data.addRows([[Number(index),0,false,'No measures with this speed',0,false,'No measures with this speed']]);
    }

	//Get providers
	var providerPromise;
	if(!$rootscope.providers) {
		//Definition of providers array
	    $rootscope.providers = [];
		providerPromise = providersResource.query({id:$rootscope.userId}, function (providersList) {
			if(providersList.length>0){
				for (var i = 0; i < providersList.length; i++) {
					$rootscope.providers.push({typeName: providersList[i], typeValue: providersList[i]});
				}
				if (!$routeParams.provider || $routeParams.provider == "unknown")
					$scope.currentProvider = $rootscope.providers[0];
				else {
					var index = 0;
					for (var i = 0; i < $rootscope.providers.length; i++) {
						if ($rootscope.providers[i].typeName == $routeParams.provider) {
							index = i;
							break;
						}
					}
					$scope.currentProvider = $rootscope.providers[index];
				}
			}
		});
	}else{
		//define the selected provider when the providers are already loaded
		if($rootscope.providers.length>0){
			if ($routeParams.provider == undefined)
				$scope.currentProvider = $rootscope.providers[0];
			else {
				var index = 0;
				for (var i = 0; i < $rootscope.providers.length; i++) {
					if ($rootscope.providers[i].typeName == $routeParams.provider) {
						index = i;
						break;
					}
				}
				$scope.currentProvider = $rootscope.providers[index];
			}
		}
	}
	
	if($rootscope.authenticated){
		if(providerPromise){
			providerPromise.$promise.then(getFrequencies);
			providerPromise.$promise.then(getGlobalFrequencies);

		}else{
			getGlobalFrequencies();
			getFrequencies();
		}
	}
	
	$scope.setRealTime = function(){};
	
	$scope.setGlobalFrequencies = function(){
		if($scope.chart.globalFrequency){
			if(providerPromise){
				providerPromise.$promise.then(getGlobalFrequencies);
			}else{
				getGlobalFrequencies();
			}
		}
		
		if(!$rootscope.global || $rootscope.global == false)
			$rootscope.global = true;
		else
			$rootscope.global = false;
	}
	
	$scope.selectType = function(type) {
		$scope.offset = 0;

		if(providerPromise){
			providerPromise.$promise.then(function(data){
		        $scope.visualization = type;
		        if($scope.currentProvider)
		        	$location.path("/frequencyChart/" + $scope.currentProvider.typeName + "/" + $scope.visualization.typeName);
		        else
		        	$location.path("/frequencyChart/unknown/" + $scope.visualization.typeName);

			});
		}
		else{
			$scope.visualization = type;
			if($scope.currentProvider)
	        	$location.path("/frequencyChart/" + $scope.currentProvider.typeName + "/" + $scope.visualization.typeName);
	        else
	        	$location.path("/frequencyChart/unknown/" + $scope.visualization.typeName);
		}
    };
	
	$scope.selectProvider = function(provider){
		wsGlobal.close();
		if(providerPromise){
			providerPromise.$promise.then(function(data){
				$scope.currentProvider = provider;
		    	$location.path("/frequencyChart/" + $scope.currentProvider.typeName + "/" + $scope.visualization.typeName);
			});
		}
		else{
			$scope.currentProvider = provider;
	    	$location.path("/frequencyChart/" + $scope.currentProvider.typeName + "/" + $scope.visualization.typeName);
		}
	};
	
	//Web socket for real time charts
	if($rootscope.authenticated){
	    var ws = new WebSocket("wss://"+ $location.host() + ":" + $location.port() + "/SpeedTest/realTimeMeasures/" + $rootscope.userId + "/" + true );
	 
	    ws.onopen = function () {
	        console.log('websoket aperto');
	    };
	
	    ws.onclose = function () {
	        console.log('websocket chiuso');
	    };
	
	    ws.onmessage =  function(event) {
			console.log('Ricevuto messaggio da websocket: ' + event.data);
			if ($scope.chart.realTime) {
				var measureList = JSON.parse(event.data);
				for (i = 0; i < measureList.length; i++) {
					var measure = measureList[i];
					if ($scope.currentProvider && measure.asnum == $scope.currentProvider.typeName) {
						$scope.userCount ++;
						$scope.globalCount ++;
						if (measure.downloadSpeed > 0 && frequencies[measure.downloadSpeed] == undefined) {
							frequencies[measure.downloadSpeed] = {};
							frequencies[measure.downloadSpeed] = 1;
							
						}
						else {
							frequencies[measure.downloadSpeed] ++;
							
						}
						
						var keys = Object.keys(frequencies);
				    	for(var i = 0; i<keys.length; i++){
				    		var foundRows = $scope.chart.data.getFilteredRows([{
								column: 0,
								value: parseInt(keys[i])
							}]);
				    		if (foundRows[0] != undefined) {
				    			$scope.chart.data.setValue(foundRows[0], 1, frequencies[keys[i]]/$scope.userCount);
								$scope.chart.data.setValue(foundRows[0], 2, true);
					        	$scope.chart.data.setValue(foundRows[0], 3, "Download speed: " + keys[i] + " Mbps\nUser frequency: "+ Number(Math.round(((frequencies[keys[i]]/$scope.userCount)*100) +'e2')+'e-2') + "%");
				    		}
				    	}
						getGlobalFrequencies();

						$scope.$apply();
					}
					else{
						var newProvider = true;
						for(var i = 0; i < $rootscope.providers.length; i++){
							if($rootscope.providers[i].typeName == measure.asnum){
								newProvider = false;
							}	
						}
						if(newProvider){
							$rootscope.providers.push({typeName: measure.asnum, typeValue: measure.asnum});
							if(!$scope.currentProvider){
								
								var index = 0;
								for (var i = 0; i < $rootscope.providers.length; i++) {
									if ($rootscope.providers[i].typeName == measure.asnum) {
										index = i;
										break;
									}
								}
								$scope.currentProvider = $rootscope.providers[index];
								
								connectGlobalSocket();
								getGlobalFrequencies();

								$scope.userCount ++;
								$scope.globalCount ++;
								if (measure.downloadSpeed > 0 && frequencies[measure.downloadSpeed] == undefined) {
									frequencies[measure.downloadSpeed] = {};
									frequencies[measure.downloadSpeed] = 1;
									
								}
								else {
									frequencies[measure.downloadSpeed] ++;
									
								}
								
								var keys = Object.keys(frequencies);
						    	for(var i = 0; i<keys.length; i++){
						    		var foundRows = $scope.chart.data.getFilteredRows([{
										column: 0,
										value: parseInt(keys[i])
									}]);
						    		if (foundRows[0] != undefined) {
						    			$scope.chart.data.setValue(foundRows[0], 1, frequencies[keys[i]]/$scope.userCount);
										$scope.chart.data.setValue(foundRows[0], 2, true);
							        	$scope.chart.data.setValue(foundRows[0], 3, "Download speed: " + keys[i] + " Mbps\nUser frequency: "+ Number(Math.round(((frequencies[keys[i]]/$scope.userCount)*100) +'e2')+'e-2') + "%");
						    		}
						    	}
						    	
								getGlobalFrequencies();
								
							}
							$scope.$apply();

						}

					}
				}
			}
		};
	}
	
	//Event for responsive chart
    $(window).resize(function(){
    	 var view = new google.visualization.DataView($scope.chart.data);
         if(! $scope.chart.globalAverage) {
			 view.hideColumns([4]);
			 view.hideColumns([5]);
			 view.hideColumns([6]);
			 view.hideColumns([7]);
		 }
     	 var chart = new google.visualization.ComboChart(document.getElementById("frequencyChart"));
         chart.draw(view, $scope.chart.options);
    });
    
    //Web socket for global real time charts
	if(providerPromise){
			providerPromise.$promise.then(connectGlobalSocket);
	}else{
		connectGlobalSocket();
	}
	var wsGlobal;
	function connectGlobalSocket(){
		if($scope.currentProvider) {
			wsGlobal = new WebSocket("wss://"+ $location.host() + ":" + $location.port() + "/SpeedTest/globalRealTimeMeasures/" + $scope.currentProvider.typeName +"/frequency");
		 
			wsGlobal.onopen = function () {
		        console.log('websoket global aperto');
		    };
		
		    wsGlobal.onclose = function () {
		        console.log('websocket global chiuso');
		    };
		
		    wsGlobal.onmessage =  function(event){
		    	console.log('Ricevuto messaggio da websocket global: ' + event.data);
		    	var frequenciesList = [];
		    	frequenciesList = JSON.parse(event.data);
		    	if($scope.chart.realTime){
		    		for(var i = 0; i < frequenciesList.length; i++){
						var freq = frequenciesList[i];
			        //var freq = JSON.parse(event.data);
						$scope.globalCount = freq.total;
						var speed = freq.speed;
						var count = freq.frequency/$scope.globalCount;
						var foundRows =  $scope.chart.data.getFilteredRows([{column: 0, value: speed}]);
						if(foundRows[0] != undefined){
							$scope.chart.data.setValue(foundRows[0], 4, Number(count));
							$scope.chart.data.setValue(foundRows[0], 5, true);
							$scope.chart.data.setValue(foundRows[0], 6, "Download speed: " + speed + "\nGlobal frequency: "+ Number(Math.round((count*100)+'e2')+'e-2') + "%");
						}
						//add day in case undefined
		    		}
					$scope.$apply();

		    	}
		    };
		}
	}

    //Get frequencies from the server
	function getFrequencies(){
		if($scope.currentProvider){
			frequencyResource.query({asnum:$scope.currentProvider.typeName,id:$rootscope.userId,start:start,end:end},function(frequenciesList){
				for(var i = 0; i < frequenciesList.length; i++){
					var freq = frequenciesList[i];
				    $scope.userCount = freq.total;
				    frequencies[freq.speed] = freq.frequency;
				    var speed = freq.speed;
				    var count = freq.frequency/$scope.userCount;
				    var foundRows =  $scope.chart.data.getFilteredRows([{column: 0, value: speed}]);
					if(foundRows[0] != undefined){
						$scope.chart.data.setValue(foundRows[0], 1, Number(count));
					    $scope.chart.data.setValue(foundRows[0], 2, true);
					    $scope.chart.data.setValue(foundRows[0], 3, "Download speed: " + speed + " Mbps\nUser frequency: "+ Number(Math.round((count*100)+'e2')+'e-2') + "%");
					}
				 }    	
			});	    
		}
	}
	
	//Get global frequencies from the server
	function getGlobalFrequencies(){
		if($scope.chart.globalFrequency && $scope.currentProvider){
			globalFrequencyResource.query({asnum:$scope.currentProvider.typeName,id:$rootscope.userId,start:start,end:end},function(frequenciesList){
				for(var i = 0; i < frequenciesList.length; i++){
					var freq = frequenciesList[i];
					$scope.globalCount = freq.total;
				    var speed = freq.speed;
				    var count = freq.frequency/$scope.globalCount;
				    var foundRows =  $scope.chart.data.getFilteredRows([{column: 0, value: speed}]);
					if(foundRows[0] != undefined){
						$scope.chart.data.setValue(foundRows[0], 4, Number(count));
						$scope.chart.data.setValue(foundRows[0], 5, true);
						$scope.chart.data.setValue(foundRows[0], 6, "Download speed: " + speed + "\nGlobal frequency: "+ Number(Math.round((count*100)+'e2')+'e-2') + "%");
					}
				 }
			});
		}
	}
}])
.directive('dsfrequencyChart', function ($location) {
    return {
        restrict: 'AE',
        link: function ($scope, elm, attrs) {
            $scope.$watch('chart', function() {
            	console.log("chart changed " + $scope.chart.data);
            	
                var view = new google.visualization.DataView($scope.chart.data);
                if(! $scope.chart.globalFrequency) {
					view.hideColumns([4]);
					view.hideColumns([5]);
					view.hideColumns([6]);
				}
            	var chart = new google.visualization.ComboChart(document.getElementById("frequencyChart"));
                chart.draw(view, $scope.chart.options);
            },true);
        }
    };
})
.factory('frequencyFactory',['$resource', function ($resource) {
	var resource =  $resource('/SpeedTest/measures/userFrequencies/:asnum/:id/:start/:end', {});

	 var factory = {};
	    
	 factory.getResource = function (scope)  {
		 return resource;
	 };
	 return factory; 	// return the factory !
}])   
.factory('globalFrequencyFactory',['$resource', function ($resource) {
	var resource =  $resource('/SpeedTest/measures/globalFrequencies/:asnum/:id/:start/:end', {});

	 var factory = {};
	    
	 factory.getResource = function (scope)  {
		 return resource;
	 };
	 return factory; 	// return the factory !
}]);   
