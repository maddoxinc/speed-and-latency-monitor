angular.module('login',[])

.controller('LoginController', [ '$scope', '$rootScope', '$location','$http', '$route',function ($scope, $rootScope, $location, $http, $route) {
	$scope.flagFirst = true;
	var authenticate = function(credentials, callback) {
		var headers = credentials ? {authorization : "Basic "
			+ btoa(credentials.username + ":" + credentials.password)
		} : {};
		
		
		$http.get('/SpeedTest/user', {headers : headers}).success(function(data) {
			if (data.name) {
				$rootScope.authenticated = true;
				$rootScope.userId = data.principal.id;
				console.log(data.principal.id);
				$scope.credentials.username = data.name;
				$rootScope.user = data.name;
			} else {
				$rootScope.authenticated = false;
			}
			callback && callback();
		}).error(function(data, status) {
			if(!$scope.flagFirst)
			{
				if(status == 401)
				{
					alertify.alert('Wrong username or password!');
				}
			}
			else
			{
				$scope.flagFirst = false;
			}
			$rootScope.authenticated = false;
			callback && callback();
		});

	};
	
	authenticate();
	$scope.credentials = {};
	$scope.login = function() {

		delete $rootScope.providers;
		authenticate($scope.credentials, function() {
		if ($rootScope.authenticated) {
		  $route.reload();
		  $scope.error = false;
		} else {
		  //$location.path("/");
		  $scope.error = true;
		}
		});
	};
	
	$scope.logout = function() {

		delete $rootScope.providers;
		$http.post('/SpeedTest/logout', {}).success(function() {
		$rootScope.authenticated = false;
		delete $rootScope.user;
		  delete $rootScope.userId;
		  $route.reload();
		}).error(function(data) {
		$rootScope.authenticated = false;
		delete $rootScope.user;
		});
		};
}]);
