/* global angular */
angular.module('measureTable',['datatables','datatables.colreorder','datatables.colvis','datatables.scroller','datatables.columnfilter','datatables.bootstrap'])

.controller('MeasuresController', ['$scope', '$rootScope', '$routeParams', 'tableMeasuresFactory','DTOptionsBuilder','DTColumnBuilder', function ($scope, $rootscope, $routeParams, tableMeasuresFactory, DTOptionsBuilder, DTColumnBuilder) {
		$scope.measures = [];
		$scope.index = 0;
		$scope.currentPage=0;
		$scope.pageSize=50;
	
		
		$scope.makeRequest = function(pageSize){
			$scope.pageSizeButton = pageSize;
			$scope.pageSize = pageSize;
	        var resource = tableMeasuresFactory.getResource();
		    $scope.measures = resource.query({id:$rootscope.userId},{index:$scope.index, pagesize:$scope.pageSize}).$promise.then(function(persons){
		    	$scope.measures = persons;
		    	$scope.currentPage = $scope.index;
		    	angular.forEach($scope.measures.content,function(item){
		    		item.timestamp = moment(item.timestamp).format("DD/MM/YYYY HH:MM:ss");
		    	});
		    });
		    
		};
		
		$scope.makeRequest($scope.pageSize);
	    
	    
	    $scope.dtOptions = DTOptionsBuilder.newOptions()
	    .withColReorder()
	    .withColVis()
	    .withColVisOption('aiExclude',[0])
	    .withOption('paging', false)
	   // .withOption('searching', false)
	    .withOption('processing', true)
	    .withBootstrap()
	    .withBootstrapOptions({
	            ColVis: {
	                classes: {
	                    masterButton: 'btn btn-primary'
	                }
	            }
	        })
	    
	    $scope.dtColumnDefs = [
       	        DTColumnBuilder.newColumn('Seq. number').withTitle('Seq. number'),
    	        DTColumnBuilder.newColumn('Client address').withTitle('Client address'),
    	        DTColumnBuilder.newColumn('AS number').withTitle('AS number'),
    	        DTColumnBuilder.newColumn('Server address').withTitle('Server address'),
    	        DTColumnBuilder.newColumn('Server domain').withTitle('Server domain'),
    	        DTColumnBuilder.newColumn('Url').withTitle('Url'),
    	        DTColumnBuilder.newColumn('Size').withTitle('Size'),
    	        DTColumnBuilder.newColumn('Duration').withTitle('Duration'),
    	        DTColumnBuilder.newColumn('Download speed').withTitle('Download speed'),
    	        DTColumnBuilder.newColumn('Connect time').withTitle('Connect time'),
    	        DTColumnBuilder.newColumn('Resource type').withTitle('Resource type'),
    	        DTColumnBuilder.newColumn('Date').withTitle('Date')
	    ];
	    
	    $scope.nextPage = function(){
	    	
	    	$scope.index++;
	    	$scope.makeRequest($scope.pageSize);
	    };
	    
	    $scope.previousPage = function(){
	    	if($scope.index>0) {
	    		$scope.index--;
	    		$scope.makeRequest($scope.pageSize);
	    	}
	    };

}])

.factory('tableMeasuresFactory',['$resource', function ($resource) {

    var resource =  $resource('/SpeedTest/measures/:id/:index/pagesize/:pagesize', {id:'id', index:'@index', pagesize:'@pagesize'},{query:{isArray:false}});

    var factory = {};
    
    factory.getResource = function (scope)  {
    	return resource;
    };
        

    return factory; 	// return the factory !
}]);