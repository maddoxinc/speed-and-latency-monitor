angular.module('registration',[])

.controller('registrationController', [ '$scope', '$rootScope', '$location','$http','registrationFactory', function ($scope, $rootScope, $location, $http, registrationFactory) {

	
	$scope.registration = function(){
		if($scope.password == $scope.passwordConfirm){
			$("#registerButton").attr("disabled","disabled");
			$scope.resource = registrationFactory.getResource();
			var obj = {};
			obj.password = $scope.password;
			obj.id = '';
			obj.username = $scope.email;
			obj.name = '';
			obj.surname = '';
			obj.enable = false;
			$scope.resource.save(obj, function(response){
				if(response.resp == 'duplicate'){
					$("#registerButton").removeAttr("disabled");
					alertify.alert('Email already present!');
				}else if(response.resp == 'IOException'){
					alertify.alert('User not valid');
					$("#registerButton").removeAttr("disabled");
				}else{
					alertify.alert('An email has been sent to your email address! Click the link inside to activate your account!');
					$location.path('/');
				}
			},function(error) {
				$("#registerButton").removeAttr("disabled");
				alertify.alert('Error during the registration');
			});
			
		}
		else
		{
			$("#registerButton").removeAttr("disabled");
			alertify.alert('Password and confirm password are different');
		}
	};
}])

.factory('registrationFactory', ['$resource', function($resource){
	
	var factory = {};
	
	factory.getResource = function(){
		return $resource('/SpeedTest/users/:id');
	};
  
	return factory;
}]);