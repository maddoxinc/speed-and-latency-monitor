-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Lug 29, 2015 alle 16:31
-- Versione del server: 5.6.21
-- PHP Version: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `speedtest`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `measure`
--

CREATE TABLE IF NOT EXISTS `measure` (
`id` bigint(20) unsigned NOT NULL,
  `clientAddress` varchar(20) NOT NULL DEFAULT '',
  `asnum` varchar(50) NOT NULL DEFAULT '',
  `serverAddress` varchar(20) NOT NULL DEFAULT '',
  `serverDomain` varchar(2000) NOT NULL DEFAULT '',
  `url` varchar(2000) NOT NULL DEFAULT '',
  `size` bigint(20) NOT NULL,
  `duration` bigint(20) NOT NULL,
  `downloadSpeed` int(11) DEFAULT NULL,
  `connectTime` bigint(20) NOT NULL,
  `resourceType` varchar(2000) DEFAULT NULL,
  `uuid` bigint(20) unsigned NOT NULL,
  `timestamp` bigint(20) NOT NULL,
  `date` bigint(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `measure`
--

INSERT INTO `measure` (`id`, `clientAddress`, `asnum`, `serverAddress`, `serverDomain`, `url`, `size`, `duration`, `downloadSpeed`, `connectTime`, `resourceType`, `uuid`, `timestamp`, `date`) VALUES
(1, '127.0.0.1', 'Consortium GARR', '130.192.182.33', 'www.polito.it', 'http://www.polito.it/includes/css/jquery.cookiebar.css?updated=20150529', 0, 1438174794191, 0, -1438174794175, NULL, 3, 1438174794175, 1438128000000),
(2, '127.0.0.1', 'Consortium GARR', '130.192.182.33', 'www.polito.it', 'http://www.polito.it/includes/css/generico.css?updated=201406251040', 0, 1438174794191, 0, -1438174794175, NULL, 3, 1438174794175, 1438128000000),
(3, '127.0.0.1', 'Consortium GARR', '130.192.182.33', 'www.polito.it', 'http://www.polito.it/includes/css/home.css?updated=201407041100', 0, 1438174794191, 0, -1438174794191, NULL, 3, 1438174794191, 1438128000000),
(4, '127.0.0.1', 'Consortium GARR', '130.192.182.33', 'www.polito.it', 'http://www.polito.it/includes/css/home.css?updated=201407041100', 11244, 0, 0, 109, 'text/css', 3, 1438176672938, 1438128000000),
(5, '127.0.0.1', 'Consortium GARR', '130.192.182.33', 'www.polito.it', 'http://www.polito.it/includes/css/generico.css?updated=201406251040', 26907, 0, 0, 109, 'text/css', 3, 1438176672938, 1438128000000),
(6, '127.0.0.1', 'Consortium GARR', '130.192.182.33', 'www.polito.it', 'http://www.polito.it/includes/css/jquery.cookiebar.css?updated=20150529', 804, 0, 0, 109, 'text/css', 3, 1438176672938, 1438128000000),
(7, '127.0.0.1', 'Consortium GARR', '130.192.182.33', 'www.polito.it', 'https://www.polito.it/intranet/', 741, 0, 0, 266, 'text/html; charset=iso-8859-1', 3, 1438176683477, 1438128000000),
(8, '127.0.0.1', 'Consortium GARR', '130.192.95.68', 'idp.polito.it', 'https://idp.polito.it/idp/profile/SAML2/Redirect/SSO?SAMLRequest=fZJbT4MwGIb%2FCun9OG3M0AwS3C5cMpUM9MIbU%2BBTmpQW%2BxXRf283ps7E7LLpe%2Bj7pCtknehpNphW7uFtADTORyck0uNFQgYtqWLIkUrWAVJT0yK73dHQ9WmvlVG1EsTJEEEbruRaSRw60AXod17Dw36XkNaYHqnnjePo9kpwo1xuvKLlVaUEmNZFVN4hNPTy%2B6Ikzsa%2Bgkt2yPt186Y%2Fc9uTZ%2BtfuICTdQ8N11Db4OKeONtNQp4XEWPLBlgUMIiisA7joGZ%2B0wTVIg6Wi9jKEAfYSjRMmoSEfhDN%2FKtZGJfBnM4DGkZPxMlPK6%2B5bLh8vYykmkRIb8oyn01zHkHjcYoVkHR1AEuPxfoM9eVY9s2XpP%2FTxB%2BaM%2BxX3lnHVNjTOxu63eTWUX86mRBqXGtgBhISEC%2BdLH8%2FQvoF&RelayState=cookie%3A1438176685_2943', 0, 1438176688949, 0, -1438176688824, 'text/plain; charset=UTF-8', 3, 1438176688824, 1438128000000),
(9, '127.0.0.1', 'Consortium GARR', '130.192.95.68', 'idp.polito.it', 'https://idp.polito.it/idp/AuthnEngine', 0, 1438176689105, 0, -1438176689074, 'text/plain; charset=UTF-8', 3, 1438176689074, 1438128000000),
(10, '127.0.0.1', 'Consortium GARR', '130.192.95.68', 'idp.polito.it', 'https://idp.polito.it/idp/x509mixed-login', 6039, 0, 0, 31, 'text/html;charset=ISO-8859-1', 3, 1438176689183, 1438128000000),
(11, '127.0.0.1', 'Consortium GARR', '130.192.95.68', 'idp.polito.it', 'https://idp.polito.it/idp/login.css', 3522, 16, 0, 16, 'text/css', 3, 1438176689386, 1438128000000),
(12, '127.0.0.1', 'Consortium GARR', '130.192.95.68', 'idp.polito.it', 'https://idp.polito.it/Chpass/vendor/jquery/jquery.min.js', 95931, 15, 0, 32, 'application/javascript', 3, 1438176689386, 1438128000000),
(13, '127.0.0.1', 'Consortium GARR', '130.192.95.68', 'idp.polito.it', 'https://idp.polito.it/idp/Authn/X509Mixed/UserPasswordLogin', 0, 1438176691218, 0, -1438176691061, 'text/plain; charset=UTF-8', 3, 1438176691061, 1438128000000),
(14, '127.0.0.1', 'Consortium GARR', '130.192.95.68', 'idp.polito.it', 'https://idp.polito.it/idp/profile/SAML2/Redirect/SSO', 13095, 16, 0, 140, 'text/html;charset=UTF-8', 3, 1438176691312, 1438128000000),
(15, '127.0.0.1', 'Consortium GARR', '130.192.182.33', 'www.polito.it', 'https://www.polito.it/Shibboleth.sso/SAML2/POST', 215, 0, 0, 328, 'text/html; charset=iso-8859-1', 3, 1438176691593, 1438128000000),
(16, '127.0.0.1', 'Consortium GARR', '130.192.182.33', 'www.polito.it', 'https://www.polito.it/intranet/', 8514, 15, 0, 94, 'text/html', 3, 1438176691921, 1438128000000),
(17, '127.0.0.1', 'Consortium GARR', '130.192.182.33', 'www.polito.it', 'https://www.polito.it/includes/css/intranet.css?updated=201212041000', 1781, 0, 0, 219, 'text/css', 3, 1438176692261, 1438128000000),
(18, '127.0.0.1', 'Consortium GARR', '130.192.182.33', 'www.polito.it', 'https://www.polito.it/includes/js/ga_polito.js', 2688, 0, 0, 234, 'application/x-javascript', 3, 1438176692261, 1438128000000),
(19, '127.0.0.1', 'Consortium GARR', '130.192.182.33', 'www.polito.it', 'https://www.polito.it/includes/css/interna.css?updated=201212041000', 19789, 16, 0, 234, 'text/css', 3, 1438176692261, 1438128000000),
(20, '127.0.0.1', 'Consortium GARR', '130.192.182.33', 'www.polito.it', 'https://www.polito.it/includes/css/generico.css?updated=201212041000', 26907, 16, 0, 234, 'text/css', 3, 1438176692261, 1438128000000),
(21, '127.0.0.1', 'Consortium GARR', '130.192.182.33', 'www.polito.it', 'https://www.polito.it/intranet/dida_main.php', 0, 1438176694355, 0, -1438176694308, 'text/html', 3, 1438176694308, 1438128000000),
(22, '127.0.0.1', 'Consortium GARR', '130.192.55.142', 'login.didattica.polito.it', 'https://login.didattica.polito.it/secure/ShibLogin.php', 1324, 0, 0, 109, 'text/html; charset=iso-8859-1', 3, 1438176698830, 1438128000000),
(23, '127.0.0.1', 'Consortium GARR', '130.192.95.68', 'idp.polito.it', 'https://idp.polito.it/idp/profile/SAML2/Redirect/SSO?SAMLRequest=hVJdb4IwFP0rpO9SQKejERKmDzNxmxG2h70sFa7SpLSst%2Bzj36%2BI29yLe2ly%0A0%2FNxz8mdI29ky7LO1moLrx2g9T4aqZAdPxLSGcU0R4FM8QaQ2ZLl2d2aRX7A%0AWqOtLrUkXoYIxgqtFlph14DJwbyJEh6364TU1rbIKJX6IJRfiYpbK0rut1oK%0Aq31haV6L3U5LsLWPqGlvENHNQ14Qb%2Bk2Eor32r9KomrP2G6ibpW9kHCibqES%0ABkonnD8Qb7VMyMsEoiAMy3jqnutxvJ9UQRzM%2BimupjwAB0PsYKXQcmUT4tBX%0Ao2A2iuIiHLNxyCbhM%2FE2p8Q3QlVCHS7XsxtAyG6LYjMa4jyBwWMUByDpvC%2BZ%0AHY3NWe2XZfl31yT9v1n8aXZOz8wG55bdO%2FXVcuPg5aeXSanfFwa4hYSEhKYD%0A5e91pF8%3D%0A&RelayState=cookie%3Aa6e4c0c5&SigAlg=http%3A%2F%2Fwww.w3.org%2F2000%2F09%2Fxmldsig%23rsa-sha1&Signature=Nzghcy1ebKX7crqi4OHPKKQG6Io870PzOnjJTy66%2BVut5iiIBvcQFEAVSwSLY3pb5qEPHwjsdM%2Bh8BC8%2BAZ%2ByQpD3jImauZMHNFiGMYl3Wvdy2kBvig4UCsZlKsI3SCcYW5XtWe14wLiWna9RXkC8PAKMCwHsedkU3AtjH47iDccyuNKSxMqnInng%2BiHl4iBp0oxmRxqVjaTSkQpYTx7Vf0x0w95CHURwLok%2BxOmRrSihaN32b48IKHRB2tDwKWetLsm0qYLpbYNX7B7u1yEN29ZZ8uLDE%2FjH0tYYrYpiNc8YFjXyL%2Bo1YHo4rKpTs5J5SiEDT2pDsO1om4a6wJyFw%3D%3D', 0, 1438176699127, 0, -1438176699096, 'text/plain; charset=UTF-8', 3, 1438176699096, 1438128000000),
(24, '127.0.0.1', 'Consortium GARR', '130.192.95.68', 'idp.polito.it', 'https://idp.polito.it/idp/AuthnEngine', 0, 1438176699252, 0, -1438176699221, 'text/plain; charset=UTF-8', 3, 1438176699221, 1438128000000),
(25, '127.0.0.1', 'Consortium GARR', '130.192.95.68', 'idp.polito.it', 'https://idp.polito.it/idp/profile/SAML2/Redirect/SSO', 13156, 0, 0, 125, 'text/html;charset=UTF-8', 3, 1438176699330, 1438128000000),
(26, '127.0.0.1', 'Consortium GARR', '130.192.55.142', 'login.didattica.polito.it', 'https://login.didattica.polito.it/Shibboleth.sso/SAML2/POST', 363, 0, 0, 33, 'text/html; charset=iso-8859-1', 3, 1438176699631, 1438128000000),
(27, '127.0.0.1', 'Consortium GARR', '130.192.55.142', 'login.didattica.polito.it', 'https://login.didattica.polito.it/secure/ShibLogin.php', 0, 1438176699804, 0, -1438176699757, 'text/html; charset=UTF-8', 3, 1438176699757, 1438128000000),
(28, '127.0.0.1', 'Consortium GARR', '130.192.55.240', 'didattica.polito.it', 'https://didattica.polito.it/portal/pls/portal/sviluppo.p_redirect_login?p_requested_url=https%3A%2F%2Fdidattica.polito.it%2Fportal%2Fpls%2Fportal%2Fsviluppo.check_login_full%3Fp_final_url%3DDEV_PORTAL.home%26p_token%3DS208987%7CStudenti%7Chttps%3A%2F%2Fidp.polito.it%2Fidp%2Fshibboleth%7C1438176701%7C744625df48e0beebb2115998cf5fe23e83118f8f', 493, 0, 0, 63, 'text/html', 3, 1438176708215, 1438128000000),
(29, '127.0.0.1', 'Consortium GARR', '130.192.55.142', 'login.didattica.polito.it', 'https://login.didattica.polito.it/pls/orasso/orasso.wwsso_app_admin.ls_login?Site2pstoreToken=v3.0~4638D272~260EA32E89FE7354EA80652368ABF415~781BF6B0BC2F014E6AA377B5169ADF0A70667422851C94EB5418E0F70F15D78C4795B4FE5509A2FE3854189BF11C12A0CC4FB84C8020D41227A85C5B8791DEF8A6C5C3208D73857C040893BC17F861F79E5221040823A626AA684752FEDDE4C1BE58C0D5A0260B8E042307B089EBDBE80487E49E1E354B760FB8251B8A0C775D1E2013FB20695A93FBDD032B3D0F22AE0D6DB575B3ABF8A526C90BD4F72919D042075478611A4F30F355A63B36DD6424558106D5AFC23F269561F5825CE50DC090F8EB9568339658DF82C5B5B675C77DEC82F17F2CA584A54792664851A2132AF38C710E3B649A1F9AA2B6293C6E539A7606D1A4F9EA35B390D2052E4F9346992850CA9DB0072B7BDB5365DE1CB113DCC568B862CD2FC51602B6A3F9C6A3BB418F821B20B201F057530EF98CC244823CBB44992ADC5813EE3FAAA771192DCC68368A0BCBEBADAA3649BA3038649C09ADA85185790F0760921F41AE4949D4DADE8CB7C82B4901571AF5E3ADE91CA5AF5C25935ADB3AB79345302244F4761B9CF4216036CB5A37BEC1E9085179CE9A67F02727F8E4140F89204595E61488FBACE807A49C204E65EA6E25BB78952E8D9E4F003BCEAE824A8E360DB4C9E498B101BF8B1FF3D86C22CB6D7BB1453C90F92ED2F09E59433477535B63FA09B557A72D0F13270B0C0E6DF7E4FF2F778AC0259E72AC46B16B202FCD51A702B3194A9D78665C001CCC5CDFFC86279B429B4A46BF0B61A6E39087EC77DC8563C24A9039D19913333249AEC7C22BE65863CB5447BE6D6E79139EE8E525E092E08B34CA041525143658050188ACD20B2CAB1961A6B1878B098D4E0532ACDFED8619B15EB63692435E3DAB903A5498F5DDED145F82FC1D', 4203, 0, 0, 47, 'application/octet-stream', 3, 1438176708356, 1438128000000),
(30, '127.0.0.1', 'Consortium GARR', '130.192.55.142', 'login.didattica.polito.it', 'https://login.didattica.polito.it/sso/auth?site2pstoretoken=v3.0~4638D272~260EA32E89FE7354EA80652368ABF415~781BF6B0BC2F014E6AA377B5169ADF0A70667422851C94EB5418E0F70F15D78C4795B4FE5509A2FE3854189BF11C12A0CC4FB84C8020D41227A85C5B8791DEF8A6C5C3208D73857C040893BC17F861F79E5221040823A626AA684752FEDDE4C1BE58C0D5A0260B8E042307B089EBDBE80487E49E1E354B760FB8251B8A0C775D1E2013FB20695A93FBDD032B3D0F22AE0D6DB575B3ABF8A526C90BD4F72919D042075478611A4F30F355A63B36DD6424558106D5AFC23F269561F5825CE50DC090F8EB9568339658DF82C5B5B675C77DEC82F17F2CA584A54792664851A2132AF38C710E3B649A1F9AA2B6293C6E539A7606D1A4F9EA35B390D2052E4F9346992850CA9DB0072B7BDB5365DE1CB113DCC568B862CD2FC51602B6A3F9C6A3BB418F821B20B201F057530EF98CC244823CBB44992ADC5813EE3FAAA771192DCC68368A0BCBEBADAA3649BA3038649C09ADA85185790F0760921F41AE4949D4DADE8CB7C82B4901571AF5E3ADE91CA5AF5C25935ADB3AB79345302244F4761B9CF4216036CB5A37BEC1E9085179CE9A67F02727F8E4140F89204595E61488FBACE807A49C204E65EA6E25BB78952E8D9E4F003BCEAE824A8E360DB4C9E498B101BF8B1FF3D86C22CB6D7BB1453C90F92ED2F09E59433477535B63FA09B557A72D0F13270B0C0E6DF7E4FF2F778AC0259E72AC46B16B202FCD51A702B3194A9D78665C001CCC5CDFFC86279B429B4A46BF0B61A6E39087EC77DC8563C24A9039D19913333249AEC7C22BE65863CB5447BE6D6E79139EE8E525E092E08B34CA041525143658050188ACD20B2CAB1961A6B1878B098D4E0532ACDFED8619B15EB63692435E3DAB903A5498F5DDED145F82FC1D&appctx=&locale=&v=', 4671, 0, 0, 79, 'text/html; charset=UTF-8', 3, 1438176708418, 1438128000000),
(31, '127.0.0.1', 'Consortium GARR', '130.192.55.240', 'didattica.polito.it', 'https://didattica.polito.it/osso_login_success?urlc=v3.0~B99CFE75FE7ED38CC48010DAFF1A8BE9~DB32DFD528E947DC49DF1A0EC5C6248767A49324E041F42B127B6D2CFDB0B46728794CFD23C555254666C45CEE1262BC529480688E66F8292B601336A5D50D68E92FE05B68981BB337FA39F2AE94405D11BAF2200A82CD77A47EC1808A8557E31CDB2B7FE6D703FECC514A0748D8C0A1E9B9F916F400003F0A310844593BDFC82EE22BAC079B2B484EA8A0D5AF77123B34451D3E941B9E5F68327AC3EC61FB9250541F87B38BA8BBEA8FB1E31CC0C4138B43AE80315696AD60DF9FE8C0DE64C315052DB6358E0DF0FE611ACE53A0C0A084FB93462EDCC88759794E3558EE01F4BD01A1DF294DF96C344E4E777BABFCE7E4A987A8879E4D78D9262F5871C07D5381A605EEB1E889A19F431BDB0CC6F803BBE8548E7781EEEF12750A67F3000FAA630BAFE946F4949CA1C9E4BEAAE37957C13C5A91ABCF756B887AE8E321530A6BD1B7D9F0ABD58C57DED3D99736E8E1362D6E649F0B95F9586EB0584DE1C2DF20F8CD258695473CD4CF3BC11DC7D2804D3875144ABD40D34CBCBE8121182C78BF83882071F4EA94BD56C8B6FF1993864C2FC3815F8572E524816D625E1BD032F508676193FF738A33E19FE8B4EDB12D8376CD1B1B87DB29B8EF1D1399B155C4B65F931A9F09C1E0C9DDD26F32DD391A58335ED76E3B90474EE7BB493B0219A161D0692C72B21796C4328C22393EF2BECA835621D0CEB02C55C4BE2AEAB12884A9334B34719A3B5231B4425C8DE4B4A51F827B4D5E7764903B24CF3EB907BABB5402FB4678FDECBCD2C32751F370321EEF66B1CD1D4D9F154026F85554B66FDC04D74933D1370A0FBC6D0B5708B85FCB56F699C941F5FFF5EE851BE5F44E95E21E273794E0202A14A8C3FB5228E6EB86ED4D039995503DBE7C2ED6326A65887F77C20A950D6A087F8D0CCC13A0FB5F801579130A59714EBD24BDCD6CF09546BF08FFA61CF516F8B96F4BBEF96585132CE5360AA373509346A0180F8CC8B2E4D08FC72EAAC6B893569693AAE642A02CC421', 539, 0, 0, 31, 'text/html; charset=iso-8859-1', 3, 1438176708559, 1438128000000),
(32, '127.0.0.1', 'Consortium GARR', '130.192.55.240', 'didattica.polito.it', 'https://didattica.polito.it/portal/pls/portal/sviluppo.p_redirect_login?p_requested_url=https%3A%2F%2Fdidattica.polito.it%2Fportal%2Fpls%2Fportal%2Fsviluppo.check_login_full%3Fp_final_url%3DDEV_PORTAL.home%26p_token%3DS208987%7CStudenti%7Chttps%3A%2F%2Fidp.polito.it%2Fidp%2Fshibboleth%7C1438176701%7C744625df48e0beebb2115998cf5fe23e83118f8f', 0, 1438176708684, 0, -1438176708606, 'text/html; charset=WINDOWS-1252', 3, 1438176708606, 1438128000000),
(33, '127.0.0.1', 'Consortium GARR', '130.192.55.240', 'didattica.polito.it', 'https://didattica.polito.it/portal/pls/portal/sviluppo.check_login_full?p_final_url=DEV_PORTAL.home&p_token=S208987%7CStudenti%7Chttps://idp.polito.it/idp/shibboleth%7C1438176701%7C744625df48e0beebb2115998cf5fe23e83118f8f', 0, 1438176708731, 0, -1438176708700, 'text/html', 3, 1438176708700, 1438128000000),
(34, '127.0.0.1', 'Consortium GARR', '130.192.55.240', 'didattica.polito.it', 'https://didattica.polito.it/portal/pls/portal/DEV_PORTAL.home', 0, 1438176708762, 0, -1438176708731, 'text/html; charset=WINDOWS-1252', 3, 1438176708731, 1438128000000),
(35, '127.0.0.1', 'Consortium GARR', '130.192.55.240', 'didattica.polito.it', 'https://didattica.polito.it/portal/page/portal/home/Studente', 115495, 140, 0, 485, 'text/html; charset=windows-1252', 3, 1438176708762, 1438128000000),
(36, '127.0.0.1', 'Consortium GARR', '130.192.55.240', 'didattica.polito.it', 'https://didattica.polito.it/didattica/css/global_header.css', 4085, 0, 0, 125, 'text/css', 3, 1438176709700, 1438128000000),
(37, '127.0.0.1', 'Consortium GARR', '130.192.55.240', 'didattica.polito.it', 'https://didattica.polito.it/didattica/css/header_fluid.css', 4007, 0, 0, 125, 'text/css', 3, 1438176709700, 1438128000000),
(38, '127.0.0.1', 'Consortium GARR', '130.192.55.240', 'didattica.polito.it', 'https://didattica.polito.it/didattica/css/header_static.css', 2554, 0, 0, 140, 'text/css', 3, 1438176709700, 1438128000000),
(39, '127.0.0.1', 'Consortium GARR', '130.192.55.240', 'didattica.polito.it', 'https://didattica.polito.it/portal/pls/portal/DEV_PORTAL.wwsbr_javascript.page_js?p_language=us&p_version=9542273F617A5A80E040C0828C3754C0', 16380, 16, 0, 140, 'application/x-javascript', 3, 1438176709700, 1438128000000),
(40, '127.0.0.1', 'Consortium GARR', '130.192.55.240', 'didattica.polito.it', 'https://didattica.polito.it/portalForm.js', 10952, 0, 0, 47, 'application/x-javascript', 3, 1438176709840, 1438128000000),
(41, '127.0.0.1', 'Consortium GARR', '130.192.55.240', 'didattica.polito.it', 'https://didattica.polito.it/didattica.css', 29682, 16, 0, 187, 'text/css', 3, 1438176709700, 1438128000000),
(42, '127.0.0.1', 'Consortium GARR', '130.192.55.240', 'didattica.polito.it', 'https://didattica.polito.it/jscript/wz_tooltip2.js', 31913, 16, 0, 63, 'application/x-javascript', 3, 1438176709840, 1438128000000),
(43, '127.0.0.1', 'Consortium GARR', '130.192.55.240', 'didattica.polito.it', 'https://didattica.polito.it/jquery/jquery-1.8.3.min.js', 93637, 109, 0, 79, 'application/x-javascript', 3, 1438176709840, 1438128000000),
(44, '127.0.0.1', 'Consortium GARR', '130.192.55.240', 'didattica.polito.it', 'https://didattica.polito.it/pls/portal30/sviluppo.chiama_materia?cod_ins=02JGROV&incarico=202954', 0, 1438176711809, 0, -1438176711778, 'text/html', 3, 1438176711778, 1438128000000),
(45, '127.0.0.1', 'Consortium GARR', '130.192.55.240', 'didattica.polito.it', 'https://didattica.polito.it/pls/portal30/sviluppo.pagina_corso.main', 13038, 0, 0, 47, 'text/html; charset=WINDOWS-1252', 3, 1438176711825, 1438128000000),
(46, '127.0.0.1', 'Consortium GARR', '130.192.55.240', 'didattica.polito.it', 'https://didattica.polito.it/pls/portal30/sviluppo.pagina_corso.main?t=3', 22237, 16, 0, 265, 'text/html; charset=WINDOWS-1252', 3, 1438176714382, 1438128000000),
(47, '127.0.0.1', 'Consortium GARR', '130.192.55.240', 'didattica.polito.it', 'https://didattica.polito.it/pls/portal30/sviluppo.materiale.sx_liv_corso?inc=202954&nod=32456417&pnod=32415679&doc=2235&rand=1', 5130, 0, 0, 47, 'text/html; charset=WINDOWS-1252', 3, 1438176717866, 1438128000000),
(48, '127.0.0.1', 'Consortium GARR', '130.192.55.240', 'didattica.polito.it', 'https://didattica.polito.it/pls/portal30/sviluppo.materiale.download?nod=32470289', 5536222, 1626, 27, 93, 'application/vnd.openxmlformats-officedocument.presentationml.presentation', 3, 1438176726162, 1438128000000),
(49, '127.0.0.1', 'Consortium GARR', '130.192.55.240', 'didattica.polito.it', 'https://didattica.polito.it/pls/portal30/sviluppo.materiale.download?nod=32470289', 5536222, 1516, 29, 250, 'application/vnd.openxmlformats-officedocument.presentationml.presentation', 3, 1438179188010, 1438128000000),
(50, '127.0.0.1', 'Consortium GARR', '130.192.55.240', 'didattica.polito.it', 'https://didattica.polito.it/pls/portal30/sviluppo.materiale.sx_liv_corso?inc=202954&nod=32456417&pnod=32415679&doc=2235&rand=6', 5130, 1, 0, 45, 'text/html; charset=WINDOWS-1252', 3, 1438179354305, 1438128000000),
(51, '127.0.0.1', 'Consortium GARR', '130.192.55.240', 'didattica.polito.it', 'https://didattica.polito.it/pls/portal30/sviluppo.materiale.download?nod=32470078', 485828, 250, 0, 78, 'application/zip', 3, 1438179355982, 1438128000000),
(52, '127.0.0.1', 'Consortium GARR', '130.192.55.240', 'didattica.polito.it', 'https://didattica.polito.it/pls/portal30/sviluppo.materiale.download?nod=32470289', 5536222, 1797, 24, 24, 'application/vnd.openxmlformats-officedocument.presentationml.presentation', 3, 1438179360514, 1438128000000);

-- --------------------------------------------------------

--
-- Struttura della tabella `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` bigint(20) unsigned NOT NULL,
  `username` varchar(50) NOT NULL DEFAULT '',
  `password` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(20) NOT NULL DEFAULT '',
  `surname` varchar(20) NOT NULL DEFAULT '',
  `enabled` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `name`, `surname`, `enabled`) VALUES
(1, 'admin', 'admin', 'admin', 'admin', 1),
(2, 'user', 'user', 'user', 'user', 1),
(3, 'luca', 'bruno', 'luca', 'bruno', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `user_roles`
--

CREATE TABLE IF NOT EXISTS `user_roles` (
`id` bigint(20) unsigned NOT NULL,
  `user` bigint(20) unsigned NOT NULL,
  `role` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `user_roles`
--

INSERT INTO `user_roles` (`id`, `user`, `role`) VALUES
(4, 1, 'ROLE_ADMIN'),
(5, 2, 'ROLE_USER'),
(6, 3, 'ROLE_USER');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `measure`
--
ALTER TABLE `measure`
 ADD PRIMARY KEY (`id`), ADD KEY `users_measures` (`uuid`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
 ADD PRIMARY KEY (`id`), ADD KEY `user_roles` (`user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `measure`
--
ALTER TABLE `measure`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user_roles`
--
ALTER TABLE `user_roles`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `measure`
--
ALTER TABLE `measure`
ADD CONSTRAINT `users_measures` FOREIGN KEY (`uuid`) REFERENCES `users` (`id`);

--
-- Limiti per la tabella `user_roles`
--
ALTER TABLE `user_roles`
ADD CONSTRAINT `user_roles` FOREIGN KEY (`user`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
